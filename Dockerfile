FROM rocker/rstudio:4.1.0

RUN apt-get update \
  && apt-get install -y --no-install-recommends \
  libxml2 \
  git

# cairo
RUN apt-get install -y --no-install-recommends \
  libx11-dev libcairo2-dev libxt-dev

# avoid:
# configure: error: gdal-config not found or not executable.
# ERROR: configuration failed for package ‘sf’
RUN apt install -y --no-install-recommends libgdal-dev

# avoid:
# libudunits2.so.0: cannot open shared object file: No such file or directory
RUN apt install -y libudunits2-dev

# clean
RUN apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# install conda
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH /opt/conda/bin:$PATH

RUN wget https://repo.anaconda.com/miniconda/Miniconda3-py39_4.9.2-Linux-x86_64.sh -O ~/miniconda.sh && \
  bash ~/miniconda.sh -b -p /opt/conda && \
  rm ~/miniconda.sh && \
  ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
  echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc


# channels
RUN conda config --add channels defaults \
  && conda config --add channels bioconda \
  && conda config --add channels conda-forge

# default conda env
RUN conda create -n tnphyto python==3.9 biopython==1.78 pandas==1.2.3 openpyxl==3.0.7 scipy==1.6.3 pysam==0.16.0.1 pytest -y
RUN echo "conda activate tnphyto" >> /home/rstudio/.profile

# transit
RUN conda create -n transit transit==3.2.1 -y

# cutadapt
RUN conda create -n cutadapt cutadapt==3.4
# samtools
RUN conda create -n samtools samtools==1.12
# deeptools
RUN conda create -n deeptools deeptools==3.5.1
# bowtie
RUN conda create -n bowtie bowtie==1.3.0
# fastx_toolkit
#RUN conda create -n fastx_toolkit fastx_toolkit==0.0.14

# clean
RUN conda clean --all

# R
RUN install2.r --error --skipinstalled \
  --deps TRUE \
  tidyverse \
  argparse \
  plotly \
  knitr \
  furrr \
  shinyFiles \
  DT \
  formattable \
  shinydashboard \
  shinybusy \
  kableExtra \
  shinyjs


# shinyCircos
RUN R -e "install.packages('circlize')"
RUN R -e "install.packages('RColorBrewer')"
RUN R -e "install.packages('data.table')"
RUN R -e "install.packages('RLumShiny')"
RUN R -e "install.packages('BiocManager');"
RUN R -e "library('BiocManager'); BiocManager::install('GenomicRanges')"

WORKDIR /home/rstudio
RUN git clone https://github.com/YaoLab-Bioinfo/shinyCircos.git
RUN chown -R rstudio shinyCircos

# sqlite
RUN apt-get update
RUN apt-get install -y sqlite3 libsqlite3-dev
RUN install2.r --error --skipinstalled --deps TRUE RSQLite

# viz
RUN install2.r --error --skipinstalled --deps TRUE shinyvalidate

# clean
RUN apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# copy the code and update sys env variables
COPY src /home/rstudio/pipeline/
ENV PATH "$PATH:/home/rstudio/pipeline/"
ENV PYTHONPATH "$PYTHONPATH:/home/rstudio/pipeline/"

# shiny
RUN /rocker_scripts/install_shiny_server.sh
COPY src/shiny/shiny-server.conf /etc/shiny-server/shiny-server.conf
COPY src/shiny/index.html /etc/shiny-server/index.html



WORKDIR /home/rstudio
