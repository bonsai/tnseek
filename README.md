# Build docker image
```bash
docker build --tag tnseek .
```
# Run docker image
```bash
INSTALL_DIRECTORY="/path/to/tnseek"

# development
docker run --rm -p 3838:3838 -p 8787:8787 --cpus $(nproc) \
-v /etc/localtime:/etc/localtime:ro \
-v "${INSTALL_DIRECTORY}/playground/":/home/rstudio/playground/ \
-v "${INSTALL_DIRECTORY}/src/":/home/rstudio/pipeline/ \
-e USERID=$UID -e PASSWORD=tnseek \
--name tnseek \
tnseek
```

# Available services

When the container is up, you will have access to:
* a shiny server: http://localhost:3838/
* a rstudio server: http://localhost:8787/


# pytest
From rstudio:
```
(tnphyto) rstudio@49976e7119cd:~/pipeline$ python -m pytest -s -vv
```
