import logging


def configure(verbose=False):
    """
    Two modes are available, verbose and non-verbose.
    """
    level = logging.INFO
    formatter = '%(name)-15s: %(levelname)-8s %(message)s'

    if verbose:
        level = logging.DEBUG
        formatter = '%(asctime)s %(name)-15s %(levelname)-8s %(message)s'

    logging.basicConfig(level=level,
                        format=formatter,
                        datefmt='%m-%d %H:%M',
                        filemode='w',
                        force=True)

# def configure(path):
#     # set up logging to file - see previous section for more details
#     logging.basicConfig(level=logging.DEBUG,
#                         format='%(asctime)s %(name)-15s %(levelname)-8s %(message)s',
#                         datefmt='%m-%d %H:%M',
#                         filename=path,
#                         filemode='w',
#                         force=True)
#
#     # define a Handler which writes INFO messages or higher to the sys.stderr
#     console = logging.StreamHandler()
#     console.setLevel(logging.INFO)
#     # set a format which is simpler for console use
#     formatter = logging.Formatter('%(name)-15s: %(levelname)-8s %(message)s')
#     # tell the handler to use this format
#     console.setFormatter(formatter)
#     # add the handler to the root logger
#     logging.getLogger().addHandler(console)
