#!/usr/bin/env python3


import os
import sys

from Bio import SeqIO
import re


class Fasta(object):
    def __init__(self, path):
        self.sequence = None

        with open(path) as handle:
            records = list(SeqIO.parse(handle, "fasta"))
            assert len(records) == 1
            record = records[0]
            self.sequence = str(record.seq)

    def get_sequence(self):
        return self.sequence

    def get_ta_positions(self):
        """leftmost 1-based positions of TAs"""
        return [m.start() + 1 for m in re.finditer('TA', self.sequence.upper())]
