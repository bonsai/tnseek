import re
import os


def parse_cutadapt_logs(f):
    """
    we do not use a defaultdict because it can be coerced to an object in R
    """
    results = dict()

    regexes = {
        "c_reads_processed": "Total reads processed:\s*([\d,]+)",
        "c_reads_with_adapters": "Reads with adapters:\s*([\d,]+)",
        "c_reads_passing_filters": "Reads written \(passing filters\):\s*([\d,]+)",
    }
    for k in regexes.keys():
        results[k] = 0

    if not os.path.exists(f):
        return results

    with open(f, 'r') as fh:
        for l in fh:
            for k, r in regexes.items():
                match = re.search(r, l)
                if match:
                    results[k] = int(match.group(1).replace(",", ""))
    return results


def parse_bowtie_logs(f):
    results = dict()
    regexes = {
        "b_reads_processed": r"# reads processed:\s+(\d+)",
        "b_reads_aligned": r"reads with at least one alignment:\s+(\d+)",
        # "reads_aligned_percentage": r"reads with at least one alignment:\s+\d+\s+\(([\d\.]+)%\)",
        "b_reads_not_aligned": r"# reads that failed to align:\s+(\d+)",
        # "not_aligned_percentage": r"# reads that failed to align:\s+\d+\s+\(([\d\.]+)%\)",
        "b_reads_multimapped_skip": r"# reads with alignments suppressed due to -m:\s+(\d+)",
        "b_reported_alignments": r"Reported\s+(\d+)\s+alignments"
        # "multimapped_percentage": r"# reads with alignments suppressed due to -m:\s+\d+\s+\(([\d\.]+)%\)",
    }
    for k in regexes.keys():
        results[k] = 0

    if not os.path.exists(f):
        return results

    with open(f, 'r') as fh:
        for l in fh:
            for k, r in regexes.items():
                match = re.search(r, l)
                if match:
                    results[k] = float(match.group(1).replace(",", ""))
    return results


def parse_build_insertion_table_logs(f):
    results = dict()
    regexes = {
        "r_reads_mapped_to_TA_sites": r"Bona fide count:\s+(\d+)"
    }
    for k in regexes.keys():
        results[k] = 0

    if not os.path.exists(f):
        return results

    with open(f, 'r') as fh:
        for l in fh:
            for k, r in regexes.items():
                match = re.search(r, l)
                if match:
                    results[k] = int(match.group(1))
    return results


def parse_build_loess_insertion_table_logs(f):
    results = dict()
    regexes = {
        "l_reads_mapped_to_TA_sites": r"Total count after normalization:\s+([\d\.]+)"
    }
    for k in regexes.keys():
        results[k] = 0

    if not os.path.exists(f):
        return results

    with open(f, 'r') as fh:
        for l in fh:
            for k, r in regexes.items():
                match = re.search(r, l)
                if match:
                    results[k] = float(match.group(1))
    return results
