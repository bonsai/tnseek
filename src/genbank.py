#!/usr/bin/env python3


import os
import sys

from Bio import SeqIO
import re
import logging

logger = logging.getLogger(__name__)


class GenBank(object):
    def __init__(self, genbank):
        if not os.path.isfile(genbank):
            logger.fatal("Invalid path: {}".format(genbank))
            sys.exit(-1)

        self.genbank = genbank
        self.features = list()
        self._parse()

    def _parse(self):
        logger.info("Parsing genbank")
        logger.debug("file: %s", self.genbank)
        seq_records = list(SeqIO.parse(self.genbank, "genbank"))
        if len(seq_records) > 1:
            logger.fatal("Multi-references is not supported: {}".format(self.genbank))
            sys.exit(-1)
        self.seq_record = seq_records[0]

        logger.info("We keep only features of the type 'CDS'")
        for f in self.seq_record.features:
            if f.type.upper() != "CDS": continue

            if "locus_tag" not in f.qualifiers.keys():
                logger.warning("Skipping lines that do not contain 'locus_tag'")
                continue

            locus_tag = f.qualifiers['locus_tag']

            # if "pseudo" in f.qualifiers.keys():
            #    print("warning: skipping pseudogenes. locus_tag:%s" % locus_tag)
            #    continue # ignore pseudogenes

            if len(locus_tag) > 1:
                logger.warning("More than one gene ID. Keep only the first one. locus_tag:%s" % str(locus_tag))
            locus_tag = locus_tag[0].replace('\t', ' ')

            gene = f.qualifiers.get('gene', [''])
            if len(gene) > 1:
                logger.warning("More than one gene name. Keep only the first one. locus_tag:%s" % locus_tag)
                logger.debug("Gene names: %s", str(gene))
            gene = gene[0].replace('\t', ' ')

            desc = f.qualifiers.get('product', ['-'])
            if len(desc) > 1:
                logger.warning("More than one gene function. Keep only the first one. locus_tag:%s" % locus_tag)
                logger.debug("Gene functions: %s", str(desc))
            desc = desc[0].replace('\t', ' ')

            if len(f.location.parts) > 1:
                logger.warning(
                    'warning: disjoint feature (spanning multiple locations). the locus_tag will be ignored:%s' % locus_tag)
                continue

            for idx, part in enumerate(f.location.parts):
                if not part.strand:
                    logger.warning('Skipping feature with no strand information:%s' % locus_tag)
                    continue
                strand = '+' if part.strand == 1 else '-'

                start = int(
                    part.start) + 1  # prot_talble, 1 based fully closed coord. Ex: prot_table [1, 10]. biopython [0, 10[.
                end = int(part.end)
                size = int(abs(end - start + 1) / 3)

                d = dict()
                d['desc'] = desc
                d['start'] = start
                d['end'] = end
                d['strand'] = strand
                d['size'] = size
                d['gene'] = gene
                d['locus_tag'] = locus_tag
                self.features.append(d)

    def convert_to_prottable(self, prottable, force=False):
        outdir = os.path.dirname(prottable)
        os.makedirs(outdir, exist_ok=True)

        if force or not os.path.exists(prottable):
            logger.info(f'Start conversion of the "genbank" file to "prot_table"')
            logger.debug(f'prot_table: {prottable}')
            with open(prottable, 'w') as handler:
                for feature in self.features:
                    vals = [feature['desc'], feature['start'], feature['end'], feature['strand'], feature['size'], '-',
                            '-',
                            feature['gene'], feature['locus_tag'], '-']
                    vals = [str(v) for v in vals]
                    print('\t'.join(vals), file=handler)
        else:
            logger.warning('The "prot_table" file already exists. No conversion will be done')
            logger.debug(f'prot_table: {prottable}')

    def as_fasta(self, path, force=False):
        outdir = os.path.dirname(path)
        os.makedirs(outdir, exist_ok=True)

        if force or not os.path.exists(path):
            logger.info(f'Start conversion of the "genbank" file to "fasta"')
            logger.debug(f'fasta: {path}')
            with open(path, 'w') as h:
                print(self.seq_record.format("fasta"), file=h)
        else:
            logger.warning('The "fasta" file already exists. No conversion will be done')
            logger.debug(f'fasta: {path}')

    def get_sequence(self):
        return str(self.seq_record.seq)

    def get_ta_positions(self):
        """leftmost 1-based positions of TAs"""
        sequence = self.get_sequence()
        return [m.start() + 1 for m in re.finditer('TA', sequence.upper())]

    def get_id(self):
        if not self.seq_record.id:
            logger.fatal('The reference sequence has no id: %s ' % self.genbank)
            sys.exit("can't find refseq id")
        return self.seq_record.id

    def get_name(self):
        if not self.seq_record.name:
            logger.fatal('The reference sequence has no name: %s ' % self.genbank)
            sys.exit("can't find refseq name")
        return self.seq_record.name

    def get_locus_tag_list(self):
        lt_list = list()
        for f in self.features:
            lt_list.append(f['locus_tag'])
        return lt_list
