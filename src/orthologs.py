#!/usr/bin/env python3
import os.path
import shutil
import sys
import logging

logger = logging.getLogger(__name__)


class Orthologs:
    def __init__(self, path):
        self.genes_by_grp = dict()
        self.grp_by_gene = dict()
        self.path = path

        if not os.path.isfile(self.path):
            logger.fatal("Orthogroup file does not exist")
            sys.exit(-1)

        with open(path, 'r') as handler:
            for idx, line in enumerate(handler.readlines()):
                line = line.strip()
                if line.startswith('#'): continue
                if not line: continue
                fields = line.split('\t')
                fields = [f.strip() for f in fields]

                ortho_grp_id = fields[0]
                grp = [g.strip() for g in fields[1:]]
                # grp = [lt.strip() for lt in grp.split()]
                if ortho_grp_id in self.genes_by_grp.keys():
                    logger.fatal("Duplicated orthologous group ID. Line %s" % (idx + 1))
                    sys.exit(-1)

                genes = list()
                for gene in grp:
                    if ':' in gene:  # Pseudomonas_syringae_USA007:fig|6666666.726844.peg.3165
                        _, gene = gene.split(':')
                    genes.append(gene)

                grp = genes
                self.genes_by_grp[ortho_grp_id] = grp

                for gene in grp:
                    if gene in self.grp_by_gene.keys():
                        logger.fatal("The following feature is in multiple ortholog group. Feature: %s", gene)
                        sys.exit(-1)
                    self.grp_by_gene[gene] = ortho_grp_id

    def get_orthologous_group(self, gene):
        return self.grp_by_gene.get(gene, None)

    def get_genes(self, orthologous_group_id):
        return self.genes_by_grp.get(orthologous_group_id, [])

    def convert(self, force=False):
        tsv = os.path.join(os.path.dirname(self.path), 'orthogrps_two_cols.tsv')
        if force or not os.path.exists(tsv):
            logger.info(f'Start conversion of the ortholog file')
            with open(tsv, 'w') as handler:
                for grp, genes in self.genes_by_grp.items():
                    for gene in genes:
                        print(f'{grp}\t{gene}', file=handler)

        else:
            logger.warning('The "ortho_table" file on two columns already exists. No conversion will be done')
            logger.debug(f'Ortho file: {tsv}')

if __name__ == '__main__':
    import sys
    p = sys.argv[1]
    o = Orthologs(p)
    o.convert()


