#!/usr/bin/env python3

import numpy as np
import os
import pandas as pd
import subprocess
import sys
import fasta as fasta_utils
import collections
import pysam

import runner
import wiggle as wiggle_utils
import genbank as genbank_utils
import logging
import gzip

logger = logging.getLogger(__name__)

TRANSPOSON = 'ACAGGYTGGATGATAAGTCCCCGGTCT'
MAX_ERROR_RATE = 0.05
MIN_OVERLAP = 7
MINIMUM_LENGTH = 15
MAXIMUM_LENTH = 20
CUT = 0


def fastq_is_empty(fq):
    """
    determine if fastq is empty
    """
    if not os.path.exists(fq):
        return True
    else:
        if fq.endswith('.gz'):
            with gzip.open(fq, 'rb') as f:
                data = f.read(1)
            return len(data) == 0
        else:
            return os.path.getsize(fq) == 0


def cutadapt(fq_file, outdir,
             transposon=TRANSPOSON,
             max_error_rate=MAX_ERROR_RATE,
             min_overlap=MIN_OVERLAP,
             minimum_length=MINIMUM_LENGTH,
             maximum_length=MAXIMUM_LENTH,
             cut=CUT):
    logger.info('Cleaning reads with cutadapt')
    os.makedirs(outdir, exist_ok=True)
    fq_file_out = os.path.join(outdir, 'clean.fastq.gz')
    too_short_fq_out = os.path.join(outdir, 'too_short.fastq.gz')
    too_long_fq_out = os.path.join(outdir, 'too_long.fastq.gz')
    untrimmed_fq_out = os.path.join(outdir, 'untrimmed.fastq.gz')

    cmd = (
        f'cutadapt -Z --cut {cut} \\\n'
        f'--adapter "{transposon};max_error_rate={max_error_rate};min_overlap={min_overlap}" \\\n'
        f'--minimum-length {minimum_length} \\\n'
        f'--maximum-length {maximum_length} \\\n'
        f'--cores $(nproc) \\\n'
        f'--too-short-output "{too_short_fq_out}" \\\n'
        f'--too-long-output "{too_long_fq_out}" \\\n'
        # f'--discard-untrimmed \\\n' # equal to --untrimmed-output /dev/null
        f'--untrimmed-output "{untrimmed_fq_out}" \\\n'
        # f'--info-file "{info_out}" \\\n'
        f'-o "{fq_file_out}" \\\n'
        f'"{fq_file}"'
    )
    runner.run(cmd, wkdir=outdir, conda_env="cutadapt", log=True)
    return fq_file_out


def bowtie(fasta, fq, outdir):
    logger.info('Mapping reads with bowtie')
    prefix = os.path.join(outdir, os.path.basename(os.path.splitext(fasta)[0]))
    bowtie_build_cmd = f'bowtie-build -q "{fasta}" "{prefix}"'
    # print("prefix", prefix)
    # print("fasta", fasta)
    runner.run(bowtie_build_cmd, wkdir=outdir, conda_env="bowtie", log=True)

    sam = os.path.join(outdir, 'aln.sam')
    unmapped_fq = os.path.join(outdir, 'unmapped.fq')
    multimapped_fq = os.path.join(outdir, 'multimapped.fq')
    bowtie_map_cmd = (f'bowtie -v 0 -m 1 --tryhard --best --threads $(nproc) \\\n'
                      f'--un "{unmapped_fq}" \\\n'
                      f'--max "{multimapped_fq}" \\\n'
                      f'-x "{prefix}" \\\n'
                      f'--sam "{fq}" \\\n'
                      f'"{sam}" \\\n'
                      )
    runner.run(bowtie_map_cmd, wkdir=outdir, conda_env="bowtie", log=True)
    return sam


def process_reads(fasta, fastq, outdir,
                  transposon=TRANSPOSON,
                  max_error_rate=MAX_ERROR_RATE,
                  min_overlap=MIN_OVERLAP,
                  minimum_length=MINIMUM_LENGTH,
                  maximum_length=MAXIMUM_LENTH,
                  cut=CUT):
    os.makedirs(outdir, exist_ok=True)
    fastq = os.path.abspath(fastq)
    outdir = os.path.abspath(outdir)

    # Clean reads
    clean_fq = cutadapt(fastq, os.path.join(outdir, '1_cutadapt'),
                        transposon,
                        max_error_rate,
                        min_overlap,
                        minimum_length,
                        maximum_length,
                        cut)

    # # collapse reads
    # collapse_outdir = os.path.join(outdir, "1_fastq_collapser")
    # collapse_fq = os.path.join(collapse_outdir,  "collapsed.fq.gz")
    # collapse_cmd = f'gunzip -c {clean_fq} | fastx_collapser -i -o {collapse_fq}'
    # runner.run(collapse_cmd, wkdir=collapse_outdir, conda_env="fastx_toolkit", log=True)

    # Align reads
    if fastq_is_empty(clean_fq):
        logger.error("Nothing left by cutadapt, stop the process of building the WIGGLE file.")
        return  # stop the process, all reads have been discarded by cutadapt

    sam = bowtie(fasta, clean_fq, os.path.join(outdir, '2_map'))

    # Generate BAM file from SAM
    logger.debug('Generating BAM file')
    bam_outdir = os.path.join(outdir, '3_bam')
    bam = os.path.join(bam_outdir, 'aln.bam')
    bam_cmd = f'samtools view --threads $(nproc) -S -b "{sam}" | samtools sort --threads $(nproc) -o "{bam}"'
    runner.run(bam_cmd, wkdir=bam_outdir, conda_env="samtools", log=True)

    # Index BAM file
    logger.debug('Indexing BAM file')
    idx_cmd = f'samtools index -@ $(nproc) "{bam}"'
    runner.run(idx_cmd, wkdir=bam_outdir, conda_env="samtools", log=False)

    # Build wig
    build_outdir = os.path.join(outdir, '4_insertions_table')
    os.makedirs(build_outdir, exist_ok=True)

    ta_pos = fasta_utils.Fasta(fasta).get_ta_positions()
    wiggle = build_wig(ta_pos, bam, build_outdir)
    # build_cmd = (f'build_insertions_table.py --ta_sites "{ta_bed}" \\\n'
    #              f'--forward {forward_bedgraph} \\\n'
    #              f'--reverse {reverse_bedgraph} \\\n'
    #              f'--wkdir {build_outdir}'
    #              )
    # runner.run(build_cmd, wkdir=build_outdir, conda_env="tnphyto", log=True)

    # Loess normalization
    # loess_outdir = os.path.join(outdir, '5_loess_insertions_table')
    # loess_cmd = f'loess.R --wig "{wiggle}" --out "{loess_outdir}"'
    # runner.run(loess_cmd, wkdir=loess_outdir, conda_env="tnphyto")

    # normalize_wiggle = os.path.join(loess_outdir, os.path.basename(wiggle))
    # return (wiggle, normalize_wiggle)
    return wiggle


def build_wig(ta_pos, bamfile, outdir):
    """
    From the alignment of the reads (bam), generate a wiggle file where each position correspond to a TA site and the
    value is the number of reads with 3' end ending on this TA site.
    A bam containing only the reads ending with TA is also built.
    """
    logger.info("Filter out all reads which 3' end is not aligned on TA dinucleotide and build the count table")
    ta_cov = {pos: 0 for pos in ta_pos}
    odered_ta_cov = collections.OrderedDict(sorted(ta_cov.items()))

    logger.debug('Parse bam : %s', bamfile)
    bam = pysam.AlignmentFile(bamfile, "rb")

    ta_aligned_bam_file = os.path.join(outdir, "ta_aligned.bam")
    ta_aligned_bam = pysam.AlignmentFile(ta_aligned_bam_file, "wb", template=bam)

    not_ta_aligned_fq_file = os.path.join(outdir, "not_ta_aligned.fq")
    not_ta_aligned_fq = open(not_ta_aligned_fq_file, "w")

    ta_aligned_wig_file = os.path.join(outdir, "ta_aligned.wig")
    ta_aligned_wig = open(ta_aligned_wig_file, "w")

    bona_fide_count = 0
    total_count = 0
    for read in bam.fetch():
        total_count += 1
        is_bona_fide = False
        if read.is_reverse:
            # if read.query_alignment_sequence.upper().startswith('TA') and read.get_reference_sequence().upper().startswith('TA'):
            ta_pos = read.get_reference_positions()[0]
            ta_pos = ta_pos + 1  # leftmost 0-based to leftmost 1-based
            if ta_pos in odered_ta_cov.keys():
                odered_ta_cov[ta_pos] += 1
                is_bona_fide = True
        else:
            # if read.query_alignment_sequence.upper().endswith('TA') and read.get_reference_sequence().upper().endswith('TA'):
            ta_pos = read.get_reference_positions()[-1]  # rightmost 0-based ta pos is equal to lefmost 1-based ta pos
            if ta_pos in odered_ta_cov.keys():
                odered_ta_cov[ta_pos] += 1
                is_bona_fide = True

        if is_bona_fide:
            bona_fide_count += 1
            ta_aligned_bam.write(read)
        else:
            qname = read.query_name
            sequence = read.query_sequence
            quality = ''.join([chr(x + 33) for x in read.query_qualities])
            print(f"@{qname}\n{sequence}\n+\n{quality}", file=not_ta_aligned_fq)

    log_file = os.path.join(outdir, "log.txt")
    with open(log_file, 'w') as log_handler:
        print('Bona fide count:', bona_fide_count, file=log_handler)
        print('Total count:', total_count, file=log_handler)

    # write wig
    logger.debug('Write wiggle file: %s', ta_aligned_wig_file)
    for p, c in odered_ta_cov.items():
        print(f'{p} {c}', file=ta_aligned_wig)

    ta_aligned_bam.close()
    bam.close()
    not_ta_aligned_fq.close()
    ta_aligned_wig.close()

    # Index BAM file
    logger.debug('Indexing BAM file')
    idx_cmd = f'samtools index -@ $(nproc) "{ta_aligned_bam_file}"'
    runner.run(idx_cmd, wkdir=outdir, conda_env="samtools", log=False)

    return ta_aligned_wig_file


if __name__ == '__main__':
    import argparse

    pass
