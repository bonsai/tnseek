#!/usr/bin/env python3

import sys
import os
import matplotlib.pyplot as plt
import scipy.stats
import numpy
import pytransit.transit_tools as transit_tools
import pytransit.tnseq_tools as tnseq_tools
import pytransit.norm_tools as norm_tools
import matplotlib.backends.backend_pdf


def run(wig, outfile):
    outdir = os.path.dirname(outfile)
    if outdir and not os.path.isdir(outdir):
        os.makedirs(outdir, exist_ok=True)
    wigList = [wig]
    (data, position) = tnseq_tools.get_data(wigList)
    #(normdata, factors) = norm_tools.normalize_data(data, norm)

    for i,reads in enumerate(data):
        #Data
        name = transit_tools.basename(wigList[i])

        nzreads = reads[reads>0]
        n_nz = len(nzreads)
        truncnzreads = sorted(nzreads)[:int(n_nz*0.99)]

        fig = plt.figure(facecolor='white', figsize=(5, 5), dpi=100)
        ax = fig.add_subplot(111, frame_on=False, rasterized=True)

        pdf = matplotlib.backends.backend_pdf.PdfPages(outfile)

        #Plot 1
        n, bins, patches = ax.hist(truncnzreads, density=1, facecolor='c', alpha=0.75, bins=100)
        plt.xlabel('Reads')
        plt.ylabel('Probability')

        plt.title('Histogram of Non-Zero Reads\nDataset: %s' % name)
        plt.grid(True)
        pdf.savefig(fig)

        #Plot 2
        plt.clf()
        ax = fig.add_subplot(111, frame_on=False, rasterized=True)
        ((qtheoretical, qdata), (slope, intercept, r)) = scipy.stats.probplot(truncnzreads, dist="geom", sparams=(1.0/numpy.mean(truncnzreads),))

        n_qdata = len(qdata)
        ax.plot(qdata, qtheoretical, "ob")

        maxval = max(numpy.max(qtheoretical), numpy.max(qdata))
        ax.plot((0, maxval), (0, maxval), ls="-", c="r")

        plt.xlabel("Data Quantiles")
        plt.ylabel("Theoretical Quantiles")
        plt.title('QQ-Plot with Theoretical Geom\nDataset: %s' % name)
        plt.grid(True)
        pdf.savefig(fig)

        #Plot 3
        plt.clf()
        ax = fig.add_subplot(111, frame_on=False, rasterized=True)
        ax.plot(sorted(reads), "ob", linewidth=3)
        plt.title('Rank Reads\nDataset: %s' % name)
        plt.xlabel("Reads")
        plt.ylabel("Rank")
        plt.grid(True)
        plt.xticks(rotation=45)
        pdf.savefig(fig)
        pdf.close()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-w", "--wiggle",
                        type=argparse.FileType('r'),
                        help="Wiggle file",
                        required=True
                        )
    parser.add_argument("-o", "--outfile",
                        type=str,
                        help="The name of the pdf to write",
                        default="transit_qc_plot.pdf",
                        )
    args = parser.parse_args()
    run(args.wiggle.name, args.outfile)