#!/usr/bin/env python3

import os
import sys

import wiggle as wiggle_utils


def run(wiggles, outfile):
    # print(wiggles)
    wiggles = [os.path.abspath(w) for w in wiggles]
    # print(wiggles)
    outfile = os.path.abspath(outfile)

    wiggle_utils.write_stats(wiggles, outfile)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description='Get simple statistics on a list of wiggle files')
    parser.add_argument("-w", "--wig",
                        type=str,
                        nargs='+',
                        help="List of wiggle files (each file must be separated by space)",
                        required=True,
                        )
    parser.add_argument("-o", "--outfile",
                        type=str,
                        help="Output file",
                        required=True
                        )
    args = parser.parse_args()
    run(args.wig, args.outfile)
