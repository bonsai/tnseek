<details><summary>Parameters</summary>

* Number of samples (permutations) to perform: ``r json$user_params$samples_number``  
* Adaptative resampling: ``r json$user_params$adaptative_resampling``  
* Pseudocount: ``r json$user_params$pseudocount``  
* Include sites with all zeros: ``r !json$user_params$exclude_rows_with_zero``  
* Ignores a fraction of the ORF, beginning at the N-terminal end  (percentage of the sequence length): ``r json$user_params$ignoreN``  
* Ignores a fraction of the ORF, beginning at the C-terminal end (percentage of the sequence length): ``r json$user_params$ignoreC``   
</details>



# Report parameters

* nb_intervals = ``r ifelse(!is.null(json$report_params$nb_intervals), json$report_params$nb_intervals, "nclass.Sturges")`` (number of intervals used in the p-value histograms) 
* qvalue_threshold = ``r json$report_params$qvalue_threshold`` (q-value threshold used in volcano plots)
* log2fc_threshold = ``r json$report_params$log2fc_threshold``  (log2 fold change threshold used in volcano plots) 

You can re-run this report with alternative parameters values by modifying the `report.json` located in the same directory and executing the following command:
```{bash rerun, eval=F, class.source = 'fold-show'}
Rscript -e 'rmarkdown::render("report.Rmd")'
```

# Conditional essential genes

<details><summary>Help</summary>
For each strain and each condition, we search for the set of conditional essential genes compared to the *control* condition. For that, we provide the following statistics, gene by gene:

* a p-value
* a q-value (correction for multiple tests computed with the Benjamini–Hochberg procedure) 
* a log2 fold change. A log2 fold change (*experiment/control*) of 1 means *experiment* is twice as large as *control*, while log2fc of 2 means *experiment* is 4x as large as *control*. Conversely, -1 means *control* is twice as large as *experiment*, and -2 means *control* is 4x as large as *experiment*. 


The computation is performed with TRANSIT using the resampling method.  
Results are available in a TSV file. We also provide the p-value histogram and a volcano plot. The p-value histogram allows checking that there is an overabundance of low p-values. The volcano plot  allows visualization of the distribution of over- or under-represented genes in any *experimental* condition compared to the *control* condition. It enables quick visual identification of genes with large fold changes. Here, we consider genes as significant if they meet two criteria: q-value ≤ ``r json$report_params$qvalue_threshold`` and abs(log2FC) ≥ ``r json$report_params$log2fc_threshold``.

In the last column of the table, another TSV file gathers all results for all conditions for a given strain. This is a convenient compilation of the individual TSV files. We also provide a global file for all conditions and strains, including orthology relationships between strains (see “overall summary” link below the table).

</details>

```{r, eval=T, error=T, message=F, results='asis'}

build_transit_resampling_path <- function(genus, strain, condition) {
    analysis <- "transit_resampling"
    glue('../2_analysis/{genus}-{strain}-{condition}-_-_-{analysis}/resampling_table.tsv')
}


build_strain_summary_path <- function(genus, strain) {
    analysis <- "transit_resampling_strain_summary"
    glue('../2_analysis/{genus}-{strain}-_-_-_-{analysis}/summary.tsv')
}


build_overall_summary_path <- function() {
    analysis <- "transit_resampling_overall_summary"
    glue('../2_analysis/_-_-_-_-_-{analysis}/summary.tsv')
}


build_hist <- function(f) {
 if (file.exists(f)) {
   class="text-primary"
   outdir <- tempfile(pattern = "hist_", tmpdir = report_wkdir_path, fileext = "")
   res <- pval_hist_from_transit_resampling_result_file(f, outdir, json$report_params$nb_intervals)
   res <- path_rel(res, start=".") #relative path
 } else {
   res <- ''
 }
  return(res)

}
vbuild_hist <- Vectorize(build_hist)

build_plot <- function(path) {
  if (! file.exists(path)) {
    link <- a(href=path_rel(path, start="."), class="text-danger", "plot")
    return(as.character(link))
  }
    outdir <- tempfile(pattern = "volcano_", tmpdir = report_wkdir_path, fileext = "")
    res <- volcano_plot_from_transit_resampling_result_file(path, outdir, json$report_params$qvalue_threshold, json$report_params$log2fc_threshold)
    link <- a(href=path_rel(res, start="."), "plot", target="_blank")
    return(as.character(link))
}
vbuild_plot <- Vectorize(build_plot)



transit_resampling_df <- df %>% distinct(genus, strain, condition, .keep_all = FALSE) %>%
  filter( toupper(condition) != "CONTROL") %>%
  mutate(result=paste(build_transit_resampling_path(genus, strain, condition)))


transit_resampling_df %>%
  arrange(strain) %>%
  mutate(genes=vbuild_link(result, 'tsv'),
         "p-value histogram"=vbuild_link(vbuild_hist(result), 'graph', external=T),
         volcano=vbuild_plot(result),
         "all conditions"=build_strain_summary_path(genus,strain),
         #"all conditions"=vbuild_link(`all conditions`, 'tsv')
         "all conditions"=vbuild_link(`all conditions`, paste0('tsv',' (', strain, ')')) # in order to "collapse_rows" works properly, we need same link name by group and version >= 1.3.4.9000 of kable
         ) %>%
  select(-result) %>%
  kable(format = "html", escape = FALSE) %>%
  kable_styling(bootstrap_options = c("striped", "hover"), full_width = T) %>%
  collapse_rows(7, valign = "middle") %>%
  scroll_box(width = "100%", extra_css="max-height:400px;")



```
[Overall summary](`r build_overall_summary_path()`)
