#!/usr/bin/env python3

import os
import sys
import subprocess
import logging

logger = logging.getLogger(__name__)


def run(cmd, wkdir, conda_env, log=True):
    # TODO: give the possibility to name the log file instead of using a bool
    os.makedirs(wkdir, exist_ok=True)

    # save the command in a file
    with open(os.path.join(wkdir, 'commands.txt'), 'a') as cmd_handler:
        print(cmd, file=cmd_handler)

    if log:
        tee = ' &> log.txt'
    else:
        tee = ''

    conda_cmd = (f'eval "$(conda shell.bash hook)" && \\\n'
                 f'conda activate {conda_env} && \\\n'
                 f'{cmd}{tee}')

    # Avoid return code to be zero when using tee:
    # When pipefail is enabled, the pipeline's return status is the value of the last (rightmost)
    # command to exit with a non-zero status, or zero if all commands exit successfully.
    conda_cmd = (f'set -o pipefail; \\\n'
                 f'{conda_cmd}')

    logger.debug("Conda environment: %s", conda_env)
    logger.debug("Working directory: %s", wkdir)
    logger.debug("Command: %s", conda_cmd)

    try:
        subprocess.run(conda_cmd, shell=True, check=True, cwd=wkdir, executable='/bin/bash')
    except subprocess.CalledProcessError as e:
        logger.fatal('The last command returned non-zero exit status {}.'.format(e.returncode))
        logger.fatal('Command: %s', cmd)
        if log:
            logger.fatal('For more details, see: %s', os.path.join(wkdir, 'log.txt'))
        sys.exit(e.returncode)
