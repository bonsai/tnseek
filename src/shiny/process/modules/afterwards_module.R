# This module is intended to:
#  * give access to the html report after the analysis is done
#  * show links to go to next analysis after the analysis is done


# UI
afterwardsReportUI <-
  function(id) {
    #affiche le lien vers le rapport
    ns <- NS(id)
    uiOutput(ns("report"))
  }

afterwardsDownstreamUI <-
  function(id) {
    #affiche les liens vers les étapes suivantes
    ns <- NS(id)
    uiOutput(ns("downstream"))
  }


# Server
afterwardsServer <- function(id, data, data_list, selectedTab) {
  moduleServer(id, function(input, output, session) {
    html_report <- reactiveVal(NULL)
    downstream_table <- reactiveVal(NULL)
    random_prefix <- NULL # prefix utilisé pour afficher le rapport HTML
    
    # Permet de changer d'onglet et de charger l'inputs_table de l'étape suivante
    # avec les résultats de l'analyse courante
    lapply(data$next_steps, function(step) {
      observeEvent(input[[step]], {
        selectedTab(step)
        l <- data_list()
        ll <- l[[step]]
        ll$inputs_table(downstream_table())
      })
    })
    
    
    # Permet de découvrir le rapport html et la table de résultats
    observe({
      req(data$outdir())
      if (file.exists(data$outdir())) {
        html_report_path <-
          file.path(data$outdir(), '/3_report/report.nb.html')
        if (file.exists(html_report_path)) {
          html_report(html_report_path)
        }
        else {
          html_report(NULL)
        }
        
        downstream_table_path <-
          file.path(data$outdir(), '4_downstream/inputs_table.tsv')
        if (file.exists(downstream_table_path)) {
          downstream_table(downstream_table_path)
        }
        else{
          downstream_table(NULL)
        }
      }
      else {
        html_report(NULL)
        downstream_table(NULL)
      }
      ## keep re-executing observer
      invalidateLater(millis = 1000, session)
    })
    
    # Quand le rapport html est disponible, on l'affiche
    output$report <- renderUI({
      if (is_null(html_report())) {
      }
      else {
        # Afin de rendre disponible le rapport HTML à l'utilisateur, nous
        # utilisons un prefixe aléatoire de tel sorte que si le fichier est
        # supprimé, le nouveau rapport affiché ne soit pas un cache de l'ancien
        # rapport
        if (! is.null(random_prefix)){
          removeResourcePath(random_prefix)
        }
        random_prefix <- stringi::stri_rand_strings(1, 10, pattern = "[A-Za-z0-9]")
        addResourcePath(random_prefix, PLAYGROUND)

        report_rel_path <-
          file.path(random_prefix,
                    fs::path_rel(html_report(), start = PLAYGROUND))
        report_link <-
          a(href = report_rel_path, "Report", target = "_blank")
        infoBox(
          "Report",
          report_link,
          icon = icon("file", lib = "glyphicon"),
          color = 'olive',
          width = NULL
        )
      }
      
    })
    
    # Quand la table de résultats est disponible, on affiche les liens vers
    # les analyses suivantes
    output$downstream <- renderUI({
      if (is_null(downstream_table())) {
      }
      else {
        tagList(lapply(data$next_steps, function(step) {
          downstream_link <- actionLink(session$ns(step), step)
          infoBox(
            "Next",
            downstream_link,
            icon = icon("random", lib = "glyphicon"),
            color = 'olive',
            width = NULL
          )
        }))
      }
    })
    
  })
}
