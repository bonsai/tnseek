# This module is not intended to be reusable.
# This module is intended to avoid namespace collision between parameters in different part of the app (process-read, core..)

# UI
coreAnalysisUI <- function(id) {
  ns <- NS(id)
  fluidRow(
    box(
      width = 12,
      title = "Inputs",
      status = "primary",
      tags$p("Enter your input file (normalized count & annotations, TSV format, required):"),
      inputFileUI(ns("inputs-table")),
      br(),
      tags$p("Enter your input file (orthology table, TSV format, optional):"),
      inputFileUI(ns("ortho-table"))
    ),
    uiOutput(ns("core_genome_ui"))
  )
}

# Server
coreAnalysisServer <- function(id) {
  moduleServer(id, function(input, output, session) {
    inputs_table_path <- inputFileServer("inputs-table")
    ortho_table_path <- inputFileServer("ortho-table")

    project_name <- reactive({
      req(inputs_table_path())
      fs::path_split(fs::path_rel(inputs_table_path(), start = PLAYGROUND))[[1]][1]
    })

    outdir <- reactive({
      outdir <- file.path(PLAYGROUND, project_name(), "core_analysis")
    })

    command <- reactive({
      inputs_table_path_val <- inputs_table_path()
      ortho_table_path_val <- ortho_table_path()

      ortho_grps_args <- ''
      if (! is.null(ortho_table_path_val)) {
        ortho_grps_args <- paste0("--ortho-grps ",  '"', ortho_table_path_val,  '" \n')
      }

      how_to_handle_rep <- input$how_to_handle_rep
      ignore_n <- input$ignore_n
      ignore_c <- input$ignore_c
      outdir_val <- outdir()
      
      verbose_option <- ''
      if (input$verbose) {
        verbose_option <- '--verbose'
      }

      glue(
        "tnphyto.py core-analysis -i \"{inputs_table_path_val}\" -o \"{outdir_val}\" \n",
        "{ortho_grps_args}",
        "--rep {how_to_handle_rep} \n",
        "--ignoreN {ignore_n} \n",
        "--ignoreC {ignore_c} \n",
        "{verbose_option} \n"
      )
    })

    output$command <- renderText(
      command()
    )
    runBackgroundJobServer("core-analysis", command, outdir)

    output$core_genome_ui <- renderUI({
      # This UI will be rendered when the inputs are set by the user
      # Basically, it contains the parameters used for the analysis and a button to run it.
      req(inputs_table_path())
      tagList(
        box(
          width = 12,
          title = "Parameters",
          status = "primary",
          collapsible = T,
          fluidRow(
            column(
              width = 5,
              selectInput(session$ns("how_to_handle_rep"),
                label = "Replicates",
                choices = list("Sum" = "SUM", "Mean" = "MEAN"),
                selected = "Mean"
              ),
              helpText(
                "Determines how to handle replicates and their read-counts"
              ),
                tags$label("Verbosity:"),
                checkboxInput(
                  session$ns("verbose"),
                  label="verbose"
                ),
                helpText("Display more informations.")
              ),
            column(
              width = 5,
              offset = 1,
              sliderInput(
                session$ns("ignore_n"),
                label = "Ignore N-terminus %",
                min = 0,
                max = 20,
                step = 1,
                value = 0
              ),
              helpText("Ignores a fraction of the ORF, beginning at the N-terminal end. Useful for ignoring read-counts that may occur at the terminal ends, even though they do not truly disrupt a genes function."),
              sliderInput(
                session$ns("ignore_c"),
                label = "Ignore c-terminus %",
                min = 0,
                max = 20,
                step = 1,
                value = 0
              ),
              helpText("Ignores a fraction of the ORF, beginning at the C-terminal end. Useful for ignoring read-counts that may occur at the terminal ends, even though they do not truly disrupt a genes function."),
            ),
          ),
        ),
        box(
          width = 12,
          title = "Command",
          status = "primary",
          collapsible = T,
          collapsed = F,
          verbatimTextOutput(session$ns("command")),
          runButtonUI(session$ns("core-analysis"), "Compute core genome"),
        ),
        box(
          width = 12,
          title = "Logs",
          status = "primary",
          collapsible = T,
          collapsed = T,
          runLogUI(session$ns("core-analysis"))
        )
      )
    })
    list(
      inputs_table = inputs_table_path, # to be able to set this value from outside
      outdir = outdir,
      next_steps = c()
    )
  })
}
