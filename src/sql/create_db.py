import os
import sys
import logging
import contextlib
import sqlite3
import glob
import re
import bisect
import pandas as pd
from Bio.SeqUtils import GC
from Bio import SeqIO
import orthologs

logger = logging.getLogger(__name__)

@contextlib.contextmanager
def db_operations(db_name):
    """
    fonction pour gérer la connexion à la base
    """
    con = sqlite3.connect(db_name)
    con.execute('PRAGMA encoding = "UTF-8"')
    con.execute('PRAGMA foreign_keys = ON')
    yield con
    con.commit()
    con.close()


def init_database(db_name, sql_script):
    """
    création de la base et des tables
    """

    with open(sql_script, 'r') as handler, db_operations(db_name) as con:
        string = handler.read()
        con.executescript(string)


def insert_data(db_name, table_name, path_or_df):
    if path_or_df is None:
        logger.warning(f'Nothing to insert into table:"{table_name}"')
        return

    if isinstance(path_or_df, pd.DataFrame):
        df = path_or_df
    else:
        df = pd.read_csv(path_or_df, sep="\t")

    if len(df) == 0:
        logger.warning(f'Nothing to insert into table:"{table_name}"')
        return

    with db_operations(db_name) as conn:
        # strip whitespace from headers
        df.columns = df.columns.str.strip()

        if table_name == 'ortho_group':  # insert ortho group only if the locus_tag is present in table "gene"
            cur = conn.cursor()
            cur.execute("SELECT DISTINCT locus_tag from gene")
            locus_tags = [row[0] for row in cur.fetchall()]
            df = df[df['locus_tag'].isin(locus_tags)]
        df.to_sql(table_name, conn, if_exists="append", index=False)


def build_strain_and_ta_table(inputs_table_file):
    inputs_table_dir, _ = os.path.split(inputs_table_file)
    inputs_table_df = pd.read_csv(inputs_table_file, sep="\t")

    # variables qui vont contenir un dictionnaire par ligne à insérer
    strain_data = list()
    ta_data = list()

    strain_df = inputs_table_df.groupby('strain', as_index=False).first()

    for idx, row in strain_df.iterrows():
        strain_row_dict = dict()

        strain_row_dict['strain'] = row['strain']
        strain_row_dict['genus'] = row['genus']

        gb = os.path.realpath(os.path.join(inputs_table_dir, row['genbank']))  # abs path
        path_wo_ext = os.path.splitext(gb)[0]

        fasta = None
        if os.path.exists(path_wo_ext + '.fasta'):
            fasta = path_wo_ext + '.fasta'
        elif os.path.exists(path_wo_ext + '.fa'):
            fasta = path_wo_ext + '.fa'

        record = SeqIO.read(fasta, "fasta")
        sequence = str(record.seq).upper()

        strain_row_dict['sequence'] = sequence
        strain_row_dict['length'] = len(sequence)
        strain_row_dict['gc_percent'] = GC(sequence) / 100

        tas = [m.start() + 1 for m in re.finditer('TA', sequence)]
        strain_row_dict['ta_number'] = len(tas)

        strain_data.append(strain_row_dict)

        for pos in tas:
            ta_row_dict = dict()
            ta_row_dict['position'] = pos
            ta_row_dict['strain'] = row['strain']
            ta_data.append(ta_row_dict)

    strain_df = pd.DataFrame(strain_data)
    ta_df = pd.DataFrame(ta_data)
    return strain_df, ta_df


def build_gene_table(inputs_table_file, ta_df):
    inputs_table_dir, _ = os.path.split(inputs_table_file)
    inputs_table_df = pd.read_csv(inputs_table_file, sep="\t")

    df_list = list()
    strain_df = inputs_table_df.groupby('strain', as_index=False).first()
    for idx, row in strain_df.iterrows():
        strain = row['strain']
        gb = os.path.realpath(os.path.join(inputs_table_dir, row['genbank']))
        path_wo_ext = os.path.splitext(gb)[0]
        prottable = path_wo_ext + '.prot_table'

        prottable_df = pd.read_csv(prottable, sep="\t", names=(
            'description', 'start_on_reference', 'end_on_reference', 'a', 'b', 'c', 'd', 'e', 'locus_tag', 'f')
                                   )
        prottable_df['strain'] = strain
        prottable_df = prottable_df.drop(columns=['a', 'b', 'c', 'd', 'e', 'f'])

        # trop lent
        # for idx2, row2 in prottable_df.iterrows():
        #    start = row2['start_on_reference']
        #    end = row2['end_on_reference']

        #    sub_df = ta_df[(ta_df.strain == strain)]
        #    ta_number = len( sub_df[(sub_df.position >= start) & (sub_df.position <= end)] )
        #    prottable_df.at[idx2,'ta_number'] = ta_number
        #    #prottable_df.at[idx2,'ta_number'] = 0

        # calcul le nb de site TA pour une feature donnée
        sub_df = ta_df[(ta_df.strain == strain)]
        l = sorted(sub_df.loc[:, 'position'])
        for idx2, row2 in prottable_df.iterrows():
            start = row2['start_on_reference']
            end = row2['end_on_reference']

            lower_bound_i = bisect.bisect_left(l, start)
            upper_bound_i = bisect.bisect_right(l, end, lo=lower_bound_i)
            prottable_df.at[idx2, 'ta_number'] = len(l[lower_bound_i:upper_bound_i])
        df_list.append(prottable_df)
    gene_df = pd.concat(df_list)
    return gene_df

def build_ortho_group_table(ortho_group_file):
    ortho_grps_obj = orthologs.Orthologs(ortho_group_file)
    ortho_data = list()
    for grp, genes in ortho_grps_obj.genes_by_grp.items():
        for gene in genes:
            row = { 'group' : grp, 'locus_tag' : gene}
            ortho_data.append(row)
    ortho_group_df = pd.DataFrame(ortho_data)
    return ortho_group_df

def build_sample_table(inputs_table_file):
    inputs_table_df = pd.read_csv(inputs_table_file, sep="\t")

    sample_df = inputs_table_df.loc[:, ('strain', 'condition')].drop_duplicates()
    return sample_df


def build_replica_table(inputs_table_file):
    inputs_table_df = pd.read_csv(inputs_table_file, sep="\t")

    replica_df = inputs_table_df.loc[:, ('strain', 'condition', 'rep', 'pool')].drop_duplicates()
    return replica_df


def build_wiggle_table(inputs_table_file):
    df_list = list()
    inputs_table_dir, _ = os.path.split(inputs_table_file)
    inputs_table_df = pd.read_csv(inputs_table_file, sep="\t")

    for idx, row in inputs_table_df.iterrows():
        wiggle = os.path.realpath(os.path.join(inputs_table_dir, row['wiggle']))  # abs path
        wiggle_df = pd.read_csv(wiggle, sep=' ', comment='#')
        wiggle_df.columns = ('position', 'count')
        wiggle_df['strain'] = row['strain']
        wiggle_df['condition'] = row['condition']
        wiggle_df['rep'] = row['rep']
        df_list.append(wiggle_df)

    wiggle_df = pd.concat(df_list)
    return wiggle_df


def build_prediction_essentiality_table(hmm_table_files):
    df_list = list()
    for hmm_table_file in hmm_table_files:
        fname = os.path.basename(os.path.dirname(hmm_table_file))

        genus, strain, condition, rep, pool, ana = fname.split('-')

        df = pd.read_csv(hmm_table_file, sep="\t", skiprows=3, na_values='', keep_default_na=False)

        df['strain'] = strain  # strain.replace('_', ' ')
        df['condition'] = condition
        df = df.rename(columns={"#ORF": "locus_tag",
                                "ES sites": "es_sites",
                                "GD sites": "gd_sites",
                                "NE sites": "ne_sites",
                                "GA sites": "ga_sites",

                                })
        # for idx, row in df.iterrows():
        #    gene_ta_nb = int(gene_df[gene_df['locus_tag'] == row['locus_tag']]['ta_number'].iloc[0])
        #    assert row['TAs'] == gene_ta_nb

        df = df.drop(columns=['gene', 'annotation', 'TAs', 'saturation', 'mean'])
        df_list.append(df)

    if df_list:
        prediction_essentiality_df = pd.concat(df_list)
    else:
        prediction_essentiality_df = None

    return prediction_essentiality_df


def build_prediction_conditional_essentiality_table(resampling_table_files):
    df_list = list()

    for resampling_table_file in resampling_table_files:
        fname = os.path.basename(os.path.dirname(resampling_table_file))

        genus, strain, condition, rep, pool, ana = fname.split('-')

        df = pd.read_csv(resampling_table_file, sep="\t", skiprows=7, na_values='', keep_default_na=False)

        df['strain'] = strain  # strain.replace('_', ' ')
        df['condition'] = condition
        df = df.rename(columns={"#Orf": "locus_tag",
                                "log2FC": "logfc",
                                "p-value": "pval",
                                "Adj. p-value": "qval"
                                })
        df = df.filter(['strain', 'condition', 'locus_tag', 'pval', 'qval', 'logfc'])
        df_list.append(df)

    if df_list:
        prediction_conditional_essentiality_df = pd.concat(df_list)
    else:
        prediction_conditional_essentiality_df = None
    return prediction_conditional_essentiality_df


def create_database(project_directory, ortho_group_file, output_database_file, force=False):
    """
    Afin de créér la base, nous prenoms en entré, le dossier des analyses généré par notre pipeline:
    prjct/
      process_reads/
      normalize/
      core_analysis/
      condi_analysis/

    En plus de ce dossier nous prenons une table d'orthologie.
    """

    if not os.path.exists(project_directory):
        logger.fatal('The project directory does not exists: %s', project_directory)
        sys.exit('Invalid project directory')

    if force and os.path.exists(output_database_file):
        os.unlink(output_database_file)

    if os.path.exists(output_database_file):
        logger.fatal("The database already exists. Nothing will be done.")
        sys.exit("DB already exists")

    # schema de la base de donnée
    sql_init_file = os.path.join(os.path.dirname(__file__), 'sqlite_db_schema.sql')
    db = output_database_file

    # Create the database
    logger.info("Create empty database")
    logger.debug("Schema: %s", sql_init_file)
    init_database(db, sql_init_file)

    # fichiers nécessaire à l'insertion en base
    downstream_dir = os.path.join(project_directory, 'normalize', '4_downstream')
    downstream_table_file = os.path.join(downstream_dir, 'inputs_table.tsv')
    logger.info('Looking for downstream table from normalize step')
    if not os.path.exists(downstream_table_file):
        logger.fatal("Can'f find requiered file: %s", downstream_table_file)
        sys.exit('Missing required file')

    # discover predictions files
    logger.info('Looking for HMM predictions from core_analysis step')
    hmm_table_files = glob.glob(os.path.join(project_directory, 'core_analysis/2_analysis/*_hmm/hmm_table_genes.tsv'))
    logger.info('Looking for RESAMPLING predictions from condi_analysis step')
    resampling_table_files = glob.glob(
        os.path.join(project_directory, 'condi_analysis/2_analysis/*_resampling/resampling_table.tsv'))


    # STRAIN & TA
    tbl_name = 'strain'
    logger.info(f'Inserting data into {tbl_name} table')
    strain_df, ta_df = build_strain_and_ta_table(downstream_table_file)
    insert_data(db, tbl_name, strain_df)

    tbl_name = 'ta'
    logger.info(f'Inserting data into {tbl_name} table')
    insert_data(db, tbl_name, ta_df)

    # GENE
    tbl_name = 'gene'
    logger.info(f'Inserting data into {tbl_name} table')
    gene_df = build_gene_table(downstream_table_file, ta_df)
    insert_data(db, tbl_name, gene_df)

    # ORTHO_GROUP
    tbl_name = 'ortho_group'
    logger.info(f'Inserting data into {tbl_name} table')
    ortho_group_df = build_ortho_group_table(ortho_group_file)
    insert_data(db, tbl_name, ortho_group_df)

    # SAMPLE
    tbl_name = 'sample'
    logger.info(f'Inserting data into {tbl_name} table')
    sample_df = build_sample_table(downstream_table_file)
    insert_data(db, tbl_name, sample_df)

    # REPLICA
    tbl_name = 'replica'
    logger.info(f'Inserting data into {tbl_name} table')
    replica_df = build_replica_table(downstream_table_file)
    insert_data(db, tbl_name, replica_df)

    # WIGGLE
    tbl_name = 'wiggle'
    logger.info(f'Inserting data into {tbl_name} table')
    wiggle_df = build_wiggle_table(downstream_table_file)
    insert_data(db, tbl_name, wiggle_df)

    # PREDICTION_ESSENTIALITY
    tbl_name = 'prediction_essentiality'
    logger.info(f'Inserting data into {tbl_name} table')
    prediction_essentiality_df = build_prediction_essentiality_table(hmm_table_files)
    insert_data(db, tbl_name, prediction_essentiality_df)

    # PREDICTION_CONDITIONAL_ESSENTIALITY
    tbl_name = 'prediction_conditional_essentiality'
    logger.info(f'Inserting data into {tbl_name} table')
    prediction_conditional_essentiality_df = build_prediction_conditional_essentiality_table(resampling_table_files)
    insert_data(db, tbl_name, prediction_conditional_essentiality_df)

if __name__ == '__main__':
    #logging.basicConfig(level=logging.DEBUG)
    #project_directory = ""
    #ortho_group_file =  ""
    #db_out = ""
    #create_database(project_directory, ortho_group_file, db_out, force=True)
    pass