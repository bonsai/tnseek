-- Create database --
PRAGMA encoding = "UTF-8";
PRAGMA foreign_keys = ON;

-- Create tables --

CREATE TABLE `gene` (
   `locus_tag` TEXT NOT NULL,
   `name` TEXT,
   `description` TEXT,
   `start_on_reference` INTEGER NOT NULL,
   `end_on_reference` INTEGER NOT NULL,
   `ta_number` INTEGER NOT NULL,
   `strain` TEXT NOT NULL,
    PRIMARY KEY(`locus_tag`)
    CONSTRAINT fk_strain
        FOREIGN KEY (`strain`)
        REFERENCES strain(`strain`)
);


CREATE TABLE `ortho_group` (
    `group` TEXT NOT NULL,
    `locus_tag` TEXT NOT NULL,
    PRIMARY KEY(`group`, `locus_tag`),
    CONSTRAINT fk_gene
        FOREIGN KEY (`locus_tag`)
        REFERENCES gene(`locus_tag`)
);


CREATE TABLE `strain` (
    `strain` TEXT NOT NULL,
    `genus` TEXT NOT NULL,
    `sequence` TEXT NOT NULL,
    `length` INTEGER NOT NULL,
    `gc_percent` REAL NOT NULL,
    `ta_number` INTEGER NOT NULL,
    PRIMARY KEY(`strain`)
);

CREATE TABLE `sample` (
    `strain` TEXT NOT NULL,
    `condition` TEXT NOT NULL,
    PRIMARY KEY(`strain`, `condition`),
    CONSTRAINT fk_strain
        FOREIGN KEY (`strain`)
        REFERENCES strain(`strain`)
);

CREATE TABLE `replica` (
    `strain` TEXT NOT NULL,
    `condition` TEXT NOT NULL,
    `rep` INTEGER NOT NULL,
    `pool` TEXT NOT NULL,
    PRIMARY KEY(`strain`, `condition`, `rep`),
    CONSTRAINT fk_sample
        FOREIGN KEY (`strain`, `condition`)
        REFERENCES sample(`strain`, `condition`)
);


CREATE TABLE `prediction_essentiality` (
    `strain` TEXT NOT NULL,
    `condition` TEXT NOT NULL,
    `locus_tag` TEXT NOT NULL,
    `es_sites` INTEGER NOT NULL,
    `ga_sites` INTEGER NOT NULL,
    `gd_sites` INTEGER NOT NULL,
    `ne_sites` INTEGER NOT NULL,
    `call` TEXT NOT NULL,
    PRIMARY KEY(`strain`, `condition`, `locus_tag`),
    CONSTRAINT fk_sample
        FOREIGN KEY (`strain`, `condition`)
        REFERENCES sample(`strain`, `condition`),
    CONSTRAINT fk_gene
        FOREIGN KEY (`locus_tag`)
        REFERENCES gene(`locus_tag`)
);

CREATE TABLE `prediction_conditional_essentiality` (
    `strain` TEXT NOT NULL,
    `condition` TEXT NOT NULL,
    `locus_tag` TEXT NOT NULL,
    `pval` REAL NOT NULL,
    `qval` REAL NOT NULL,
    `logfc` REAL NOT NULL,
    PRIMARY KEY(`strain`, `condition`, `locus_tag`),
    CONSTRAINT fk_sample
        FOREIGN KEY (`strain`, `condition`)
        REFERENCES sample(`strain`, `condition`),
    CONSTRAINT fk_gene
        FOREIGN KEY (`locus_tag`)
        REFERENCES gene(`locus_tag`)
);

CREATE TABLE `ta` (
    `position` INTEGER NOT NULL,
    `strain` TEXT NOT NULL,
    PRIMARY KEY(`strain`, `position`),
    CONSTRAINT fk_strain
        FOREIGN KEY (`strain`)
        REFERENCES strain(`strain`)
);

CREATE TABLE `wiggle` (
    `strain` TEXT NOT NULL,
    `condition` TEXT NOT NULL,
    `rep` INTEGER NOT NULL,
    `position` INTEGER NOT NULL,
    `count` REAL NOT NULL,
    PRIMARY KEY(`strain`, `condition`, `rep`, `position`),
    CONSTRAINT fk_replica
        FOREIGN KEY (`strain`, `condition`, `rep`)
        REFERENCES replica(`strain`, `condition`, `rep`)
);


