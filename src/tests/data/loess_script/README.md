# simulated_dataset.wig
Based on a quadratic equation.  
Commands used :
```R
TA_pos_vec <- seq(10, 30, by=0.01)
count_vec <- 1/100*(TA_pos_vec-20)**2 + 10
ori_counts <- as_tibble(data.frame(TA_pos=TA_pos_vec, count=count_vec))
corrupt_vec <- count_vec + rnorm(length(count_vec), mean = 0, sd = 0.2)
counts <- as_tibble(data.frame(TA_pos=ori_counts$TA_pos, count=corrupt_vec))
counts <- counts %>% arrange(TA_pos)
write.table(counts, "/tmp/test.wig", sep = " ", quote=FALSE, col.names = FALSE, row.names = FALSE, append = TRUE)
```

# wt_read_tally_chr1.wig
dataset (chrI, reverse + forward) from https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3799429/
