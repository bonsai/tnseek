Theorical alignment

```
TA positions on ref: {1, 11, 18, 26, 32, 40, 46, 99, 117} (zero based)
    ↓ 1       ↓ 11   ↓ 18    ↓ 26  ↓ 32    ↓ 40  ↓ 46                                                 ↓ 99              ↓ 117
5’-ttatccacagataccgattaaaaaaataagattatttgagtaaatttacccatgatcccagccagacctccgcgggatcctccggaatgtcgtgctgggtaatgtcgatcttcaacatatcgccgatccgacgggcacccagatcctgcagcaattgatccactttgacgatcgcgccgc-3’
                                                                                                        atgtcgatcttcaacata→ (r1)
                                                                                                       ←atgtcgatcttcaacata (r3)
                                                                                                                        tatcgccgatccgacggg→ (r4)
                                                                                                                       ←tatcgccgatccgacggg (r6)
```
Expected coverage (zero based)
```
1 0
11 0
18 0
26 0
32 0
40 0
46 0
99 0
117  {r2, r6}
```

Expected wig (one based)
```
2 0
12 0
19 0
27 0
33 0
41 0
47 0
100 0
118  2
```
