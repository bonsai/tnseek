Theorical alignment

```
TA positions on ref: {10,37,39,116,160} (zero based)
                                          ↓39   
             ↓10                        ↓37                                                                            ↓116                                        ↓160
5’-CGGGGTGGTGTATGGCGACATTGGCACCAGCCCGCTTTATACGCTCAGGGAATGTCTTTCCGGACAGTTTGGCTTTGGGGTTGAACCCGATTCCGTCTTTGGCTTTCTCTCCCTGATATTCTGGCTGCTGGTGCTGGTGGGGGGTGCTGGGGGCGCGCAGTATGGCGACATTGGCATTT-3’
             ↑             CACCAGCCCGCTTTATA→ (r1)                                                       CTTTCTCTCCCTGATAT→ (not ok, does not ends with TA) (r10)  ↑
             ↑              ACCAGCCCGCTTTATA→ (r2)                     TTGGCTTTGGGGTTGA→ (not ok, no TA) (r11)         ↑                                           ↑
             ↑                          ↑ ↑                                                                   TCTCCCTGATATTCTG→ (not ok, does not ends with TA) (r12)
             ↑                          ↑←TACGCTCAGGGAA (r3)                                              TTTCTCTCCCTGAT→ (not ok, does not ends with TA) (r13)    ↑
             ↑                          ↑←TACGCTCAGGGAAT (r4)                                             TTTCTCTCCCTGATA→ (r14)                                   ↑
             ↑                          ↑←TACGCTCAGGGAATG (r5)                                                                                                     ↑
             ↑                         ←TATACGCTCAGGGAATG (r6)                                                                                                     ↑
            ←TATGGCGACATTGGCA (not ok, the position is not unique) (r7)                                   →→→→→→                                                  ←TATGGCGACATTGGCA (not ok)
                                        TATACGCTCAGGGAATGTCTTTCCGGACAG→ (not ok, 3' TA) (r8)
                             ←CAGCCCGCTTTATA (not ok, 3' TA) (r9)                                               
```

Image of the alignment:
![aln](aln.jpg "Alignment")

Expected coverage (zero based)
```
10  0
37  1 (r6)
39  5 (r1, r2, r3, r4, r5)
116 1 (r14)
160 0
```

Expected wig (one based)
```
11  0
38  1
40  5
117 1
161 0
```
