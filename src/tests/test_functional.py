import pytest
import sys
import os
import filecmp
import pandas as pd
import runner

DATA_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data')


@pytest.fixture(scope="session")
def data_dir():
    return DATA_DIR


# ------------------------------------------------------------------------------#
# helper functions                                                              #
# ------------------------------------------------------------------------------#
def get_hmm_prediction(df, genus, strain, condition, locus_tag):
    """
    A partir de la table résumée de plus haut niveau, retourne la prédiction qui correspond au genre, souche, condition & lt
    """
    # on cherche la ligne correspondante
    selected = df.loc[:, ('-', 'locus_tag', genus, strain)] == locus_tag

    # on retrouve son index
    idx = df.index[selected].tolist()

    # on ne doit avoir qu'une seule ligne et par conséquent qu'un seul index
    if len(idx) != 1:
        print(
            f"Can't find the HMM row corresponding to your request: genus:{genus}, strain:{strain}, condition:{condition}, locus_tag:{locus_tag}")
        return None

    # on retourne la prédiction correpondante
    idx = idx.pop()
    value = df.loc[idx, ('conditions', condition, genus, strain)]
    return value


def get_resampling_prediction(df, genus, strain, condition, locus_tag, logfc_threshold=1, qval_threshod=0.05):
    # on cherche la ligne correspondante
    selected = df.loc[:, ('-', 'locus_tag', genus, strain)] == locus_tag

    # on retrouve son index
    idx = df.index[selected].tolist()

    # on ne doit avoir qu'une seule ligne et par conséquent qu'un seul index
    if len(idx) != 1:
        print(
            f"Can't find the RESAMPLING row corresponding to your request: genus:{genus}, strain:{strain}, condition:{condition}, locus_tag:{locus_tag}")
        return None

    idx = idx.pop()
    logfc = df.loc[idx, (condition, genus, strain, "logfc")]
    qval = df.loc[idx, (condition, genus, strain, "qvalue")]

    # si les valeurs sont significative, en fonction de la variation on retourne la prédiction "GA" ou "GD"
    if pd.isna(logfc) and pd.isna(qval):
        pass
    else:
        if float(qval) <= qval_threshod:
            if float(logfc) >= logfc_threshold:
                return 'GA'
            if float(logfc) <= -logfc_threshold:
                return 'GD'
    return 'INSIGNIFICANT'


def get_expected_prediction(df, genus, strain, condition, locus_tag):
    # on cherche la ligne correspondante
    selected = (df.loc[:, 'genus'] == genus) & (df.loc[:, 'strain'] == strain) & (
            df.loc[:, 'locus_tag'] == locus_tag) & (df.loc[:, 'condition'] == condition)

    # on retrouve son index
    idx = df.index[selected].tolist()

    # on ne doit avoir qu'une seule ligne et par conséquent qu'un seul index
    # comme il y a un sous ensemble restraint de gènes dans cette table
    # nous devrions tomber dans la condition suivante assez souvent
    if len(idx) != 1:
        return None

    # on retourne la prédiction correpondante
    idx = idx.pop()
    value = df.loc[idx, 'call']
    return value


def check_predictions(hmm_df, resampling_df, expected_predictions_df):
    """
    # Caractéristiques du jeu de données simulé
    # 2 genres, chacun ayant 2 souches et cela pour 3 conditions.

    # Gènes ESSENTIELS
    # 8% des gènes sont ES (ont 0 insertion), ces 8%, pour une souche donnée sont partagés entre les conditions.
    # Pour chaque condition, on ajoute un gène ES qui propre à la condition

    # Gènes CONDI
    # Pour les conditions autre que control (conditions expérimentales), on va augmenter/diminuer les insertions (7% de UP, 4% de DOWN)
    # Il n'y a aucun gène UP/DOWN qui soit en commun entre les conditions expérimentales
    # Un gène UP a 4 fois plus d'insertions que la normale
    # Un gène DOWN a 4 fois moins d'insertions que la normale

    # A noter que nous ne testons que les gènes contenu dans la table des prédictions attendues
    # Cette table ne contient que les gènes pour lequelles une modification a été faite:
    # ES, GA, GD
    # Cela permet au moins de controler que les gènes d'intérêt ont les bonne prédiction
    """
    for idx, row in expected_predictions_df.iterrows():
        genus = row['genus']
        strain = row['strain']
        condition = row['condition']
        locus_tag = row['locus_tag']
        expected_prediction = row['call']
        hmm_prediction = get_hmm_prediction(hmm_df, genus, strain, condition, locus_tag)

        # HMM
        # De ce que j'ai pu constater la méthode hmm n'est pas très bonne pour prédire les GD & GA.
        # Nous nous cantonnons donc aux seules valeurs essentiels
        if expected_prediction == "ES":
            message = f"hmm prediction conflict. genus:{genus}, strain:{strain}, condition:{condition}, " \
                      f"locus_tag:{locus_tag}, expected_prediction:{expected_prediction}, hmm_prediction:{hmm_prediction}"
            assert expected_prediction == hmm_prediction, message

        # RESAMPLING
        if condition == 'control': continue

        expected_pred_ctrl = get_expected_prediction(expected_predictions_df, genus, strain, 'control', locus_tag)
        resampling_prediction = get_resampling_prediction(resampling_df, genus, strain, condition, locus_tag)

        # Si la valeur attendu est la même entre le control et la condition courante
        # il n'y aura pas de variation significative
        if expected_pred_ctrl == expected_prediction:
            message = f"resampling prediction conflict. genus:{genus}, strain:{strain}, condition:{condition}, " \
                      f"locus_tag:{locus_tag}, expected_prediction:INSIGNIFICANT, resampling_prediction:{resampling_prediction}"
            assert "INSIGNIFICANT" == resampling_prediction, message
        elif expected_prediction == "ES":
            # Si la valeur attendue est ES, cela veut dire que la méthode de resampling doit prédire un GD
            expected_prediction = "GD"
            message = f"resampling prediction conflict. genus:{genus}, strain:{strain}, condition:{condition}, " \
                      f"locus_tag:{locus_tag}, expected_prediction:{expected_prediction}, resampling_prediction:{resampling_prediction}"
            assert expected_prediction == resampling_prediction, message
        else:
            message = f"resampling prediction conflict. genus:{genus}, strain:{strain}, condition:{condition}, " \
                      f"locus_tag:{locus_tag}, expected_prediction:{expected_prediction}, resampling_prediction:{resampling_prediction}"
            assert expected_prediction == resampling_prediction, message


# ------------------------------------------------------------------------------#
# where we test                                                                 #
# ------------------------------------------------------------------------------#
functional_data_directory = os.path.join(DATA_DIR, "functional_test")


@pytest.mark.skipif(not os.path.exists(functional_data_directory), reason="Dataset not available")
def test_functional(data_dir, tmp_path):
    ortho_table = os.path.join(data_dir, "functional_test/ortho/OrthologousGroupsClean-simplified.tsv")
    expected_predictions = os.path.join(data_dir, "functional_test/expected/predictions.tsv")
    expected_reads = os.path.join(data_dir, "functional_test/expected/process_reads_simple_statistic.tsv")

    # PROCESS_READS
    inputs_table = os.path.join(data_dir, "functional_test/input_shuf.tsv")
    transposon = "ACAGGTTGGATGATAAGTCCCCGGTCTT"
    outdir = tmp_path / "process_reads"

    cmd = f'tnphyto.py process-reads -i "{inputs_table}" --transposon {transposon} --minimum-length 16 --maximum-length 30 --cut 5 -o "{outdir}" --force'
    runner.run(cmd, wkdir=outdir, conda_env="tnphyto", log=True)

    reads_stats = os.path.join(outdir, '3_report/workdir/simple_statistic.tsv')
    assert filecmp.cmp(reads_stats , expected_reads), 'The stats on aligned reads are wrong'


    # NORMALIZE
    # update the downstream table to apply loess & ttr on every samples
    downstream_table = os.path.join(outdir, "4_downstream", "inputs_table.tsv")
    df = pd.read_csv(downstream_table, sep='\t', dtype=str)
    df['loess'] = True
    df['ttr'] = True
    df.to_csv(downstream_table, sep="\t", index=False)

    inputs_table = downstream_table
    outdir = tmp_path / "normalize"

    cmd = f'tnphyto.py normalize -i "{inputs_table}" -o "{outdir}" --force'
    runner.run(cmd, wkdir=outdir, conda_env="tnphyto", log=True)

    inputs_table = os.path.join(outdir, "4_downstream", "inputs_table.tsv")
    # CORE ANALYSIS
    outdir = tmp_path / "core_analysis"
    cmd = f'tnphyto.py core-analysis -i "{inputs_table}" -g {ortho_table} -o "{outdir}" --force'
    runner.run(cmd, wkdir=outdir, conda_env="tnphyto", log=True)
    hmm_predictions = os.path.join(outdir, "2_analysis/_-_-_-_-_-transit_hmm_overall_summary/summary.tsv")

    # CONDI ANALYSIS
    outdir = tmp_path / "condi_analysis"
    cmd = f'tnphyto.py condi-analysis -i "{inputs_table}" -g {ortho_table} -o "{outdir}" --adaptative-resampling --force'
    runner.run(cmd, wkdir=outdir, conda_env="tnphyto", log=True)
    resampling_predictions = os.path.join(outdir, "2_analysis/_-_-_-_-_-transit_resampling_overall_summary/summary.tsv")

    # CHECK PREDICTIONS
    # parse predictions
    hmm_df = pd.read_csv(hmm_predictions, header=[0, 1, 2, 3], sep="\t", dtype=str)
    resampling_df = pd.read_csv(resampling_predictions, header=[0, 1, 2, 3], sep="\t", dtype=str)
    expected_predictions_df = pd.read_csv(expected_predictions, sep="\t", dtype=str)

    # check
    check_predictions(hmm_df, resampling_df, expected_predictions_df)
