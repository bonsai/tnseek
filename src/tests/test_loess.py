import pytest
import sys
import os
import subprocess
import numpy as np
import tempfile

DATA_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data')
LOESS_SCRIPT = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', 'loess.R')


@pytest.fixture(scope="session")
def data_dir():
    return DATA_DIR


# ------------------------------------------------------------------------------#
# LOESS NORMALIZATION                                                           #
# ------------------------------------------------------------------------------#
@pytest.mark.parametrize("wig,expected_wig", [(os.path.join(DATA_DIR, "loess_script", "simulated_dataset.wig"),
                                               os.path.join(DATA_DIR, "loess_script",
                                                            "simulated_dataset_expected_res.wig")),
                                              (os.path.join(DATA_DIR, "loess_script", "wt_read_tally_chr1.wig"),
                                               os.path.join(DATA_DIR, "loess_script",
                                                            "wt_read_tally_chr1_expected_res.wig"))])
def test_loess_norm_simulated_case(wig, expected_wig, tmp_path):
    computed_wig = tmp_path / "computed.wig"
    cmd = "Rscript --vanilla {} --wig {} --out {}".format(LOESS_SCRIPT, wig, computed_wig)
    subprocess.run(cmd, shell=True, check=True)

    normalized_wig_res = np.loadtxt(computed_wig, skiprows=0, delimiter=' ')
    expected_wig_res = np.loadtxt(expected_wig, skiprows=0, delimiter=' ')

    counts = normalized_wig_res[:, 1]
    expected_counts = expected_wig_res[:, 1]

    epsilon = 1e-7
    assert (np.linalg.norm(counts - expected_counts, ord=2) <= epsilon)
