import pytest
import sys
import os
import filecmp
import process_reads

DATA_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data')


@pytest.fixture(scope="session")
def data_dir():
    return DATA_DIR


def list_dir(d, extension=None):
    """
    Handy function
    d path is relative to the data dir
    """
    dir_of_interest = os.path.join(DATA_DIR, d)
    if extension:
        file_lst = sorted([f for f in os.listdir(dir_of_interest) if f.endswith(extension)])
    else:
        file_lst = sorted([f for f in os.listdir(dir_of_interest)])
    return [os.path.join(d, f) for f in file_lst]


# ------------------------------------------------------------------------------#
# process_reads function                                                       #
# ------------------------------------------------------------------------------#
def test_process_reads_default_case(data_dir, tmp_path):
    expected_res = os.path.join(data_dir, "process_reads_script/process_reads/default/expected_res.wig")
    fasta = os.path.join(data_dir, "process_reads_script/process_reads/default/reference.fa")
    fastq = os.path.join(data_dir, "process_reads_script/process_reads/default/reads.fq")
    outdir = tmp_path

    wiggle = process_reads.process_reads(fasta, fastq, outdir,
                                         transposon="acaggctggatgataagtccccggtct",
                                         max_error_rate=process_reads.MAX_ERROR_RATE,
                                         min_overlap=process_reads.MIN_OVERLAP,
                                         minimum_length=process_reads.MINIMUM_LENGTH,
                                         maximum_length=process_reads.MAXIMUM_LENTH,
                                         cut=0)

    assert filecmp.cmp(wiggle, expected_res)


def test_process_reads_more_realistic_case(data_dir, tmp_path):
    expected_res = os.path.join(data_dir, "process_reads_script/process_reads/more_realistic/expected_res.wig")
    fasta = os.path.join(data_dir, "process_reads_script/process_reads/more_realistic/reference.fa")
    fastq = os.path.join(data_dir, "process_reads_script/process_reads/more_realistic/reads.fq")
    outdir = tmp_path

    wiggle = process_reads.process_reads(fasta, fastq, outdir,
                                         transposon="TAACAGGCTGGATGATAAGTCCCCGGTCT",
                                         max_error_rate=process_reads.MAX_ERROR_RATE,
                                         min_overlap=process_reads.MIN_OVERLAP,
                                         minimum_length=11,
                                         maximum_length=59,
                                         cut=3)

    assert filecmp.cmp(wiggle, expected_res)
