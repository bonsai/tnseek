import pytest
import sys
import os
import filecmp
import random
import tnphyto

DATA_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data')


@pytest.fixture(scope="session")
def data_dir():
    return DATA_DIR


# ------------------------------------------------------------------------------#
# build_core_strain_lvl_summary function                                        #
# ------------------------------------------------------------------------------#

def test_build_core_strain_lvl_summary_default_case_one_condition(data_dir, tmp_path):
    expected_res = os.path.join(data_dir,
                                "ess_summary_test/strain_lvl_summary/default_case/expected_res_one_condition.tsv")
    computed_res = tmp_path / 'strain_lvl_summary.tsv'
    files = [os.path.join(data_dir,
                          'ess_summary_test/strain_lvl_summary/default_case/Dickeya-Dickeya_dadantii_3937-control-_-_-transit_hmm/hmm_table_genes.tsv')
             ]

    tnphyto.build_core_strain_lvl_summary(files, computed_res)
    assert filecmp.cmp(expected_res, computed_res), "Computed result file & expected result file differ"


def test_build_core_strain_lvl_summary_default_case_three_conditions(data_dir, tmp_path):
    expected_res = os.path.join(data_dir,
                                "ess_summary_test/strain_lvl_summary/default_case/expected_res_three_conditions.tsv")
    computed_res = tmp_path / 'strain_lvl_summary.tsv'
    files = [os.path.join(data_dir,
                          'ess_summary_test/strain_lvl_summary/default_case/Dickeya-Dickeya_dadantii_3937-control-_-_-transit_hmm/hmm_table_genes.tsv'),
             os.path.join(data_dir,
                          'ess_summary_test/strain_lvl_summary/default_case/Dickeya-Dickeya_dadantii_3937-chicory-_-_-transit_hmm/hmm_table_genes.tsv'),
             os.path.join(data_dir,
                          'ess_summary_test/strain_lvl_summary/default_case/Dickeya-Dickeya_dadantii_3937-water-_-_-transit_hmm/hmm_table_genes.tsv')
             ]
    random.shuffle(files)

    tnphyto.build_core_strain_lvl_summary(files, computed_res)
    assert filecmp.cmp(expected_res, computed_res), "Computed result file & expected result file differ"


# ------------------------------------------------------------------------------#
# build_core_overall_lvl_summary function                                       #
# ------------------------------------------------------------------------------#

def test_build_core_overall_lvl_summary_default_case(data_dir, tmp_path):
    expected_res = os.path.join(data_dir, "ess_summary_test/overall_lvl_summary/default_case/expected_res.tsv")
    computed_res = tmp_path / 'overall_summary.tsv'

    files = [
        os.path.join(data_dir,
                     'ess_summary_test/overall_lvl_summary/default_case/Pseudomonas-Pseudomonas_syringae_CC1557-chicory-_-_-transit_hmm/hmm_table_genes.tsv'),
        os.path.join(data_dir,
                     'ess_summary_test/overall_lvl_summary/default_case/Pseudomonas-Pseudomonas_syringae_TA043-control-_-_-transit_hmm/hmm_table_genes.tsv'),
        os.path.join(data_dir,
                     'ess_summary_test/overall_lvl_summary/default_case/Dickeya-Dickeya_dadantii_3937-control-_-_-transit_hmm/hmm_table_genes.tsv'),
        os.path.join(data_dir,
                     'ess_summary_test/overall_lvl_summary/default_case/Dickeya-Dickeya_zeae_A586-chicory-_-_-transit_hmm/hmm_table_genes.tsv'),
        os.path.join(data_dir,
                     'ess_summary_test/overall_lvl_summary/default_case/Pseudomonas-Pseudomonas_syringae_TA043-chicory-_-_-transit_hmm/hmm_table_genes.tsv'),
        os.path.join(data_dir,
                     'ess_summary_test/overall_lvl_summary/default_case/Dickeya-Dickeya_zeae_A586-control-_-_-transit_hmm/hmm_table_genes.tsv'),
        os.path.join(data_dir,
                     'ess_summary_test/overall_lvl_summary/default_case/Pseudomonas-Pseudomonas_syringae_CC1557-control-_-_-transit_hmm/hmm_table_genes.tsv'),
        os.path.join(data_dir,
                     'ess_summary_test/overall_lvl_summary/default_case/Dickeya-Dickeya_dadantii_3937-chicory-_-_-transit_hmm/hmm_table_genes.tsv')
    ]
    orthogrps = os.path.join(data_dir, 'ess_summary_test/overall_lvl_summary/default_case/orthogrps.tsv')

    random.shuffle(files)
    tnphyto.build_core_overall_lvl_summary(files, orthogrps, computed_res)
    assert filecmp.cmp(expected_res, computed_res), "Computed result file & expected result file differ"


def test_build_core_overall_lvl_summary_default_case_missing_file(data_dir, tmp_path):
    expected_res = os.path.join(data_dir,
                                "ess_summary_test/overall_lvl_summary/default_case/expected_res_missing_file.tsv")
    computed_res = tmp_path / 'overall_summary.tsv'

    files = [
        os.path.join(data_dir,
                     'ess_summary_test/overall_lvl_summary/default_case/Pseudomonas-Pseudomonas_syringae_CC1557-chicory-_-_-transit_hmm/does_not_exist.tsv'),
        os.path.join(data_dir,
                     'ess_summary_test/overall_lvl_summary/default_case/Pseudomonas-Pseudomonas_syringae_TA043-control-_-_-transit_hmm/hmm_table_genes.tsv'),
        os.path.join(data_dir,
                     'ess_summary_test/overall_lvl_summary/default_case/Dickeya-Dickeya_dadantii_3937-control-_-_-transit_hmm/hmm_table_genes.tsv'),
        os.path.join(data_dir,
                     'ess_summary_test/overall_lvl_summary/default_case/Dickeya-Dickeya_zeae_A586-chicory-_-_-transit_hmm/hmm_table_genes.tsv'),
        os.path.join(data_dir,
                     'ess_summary_test/overall_lvl_summary/default_case/Pseudomonas-Pseudomonas_syringae_TA043-chicory-_-_-transit_hmm/hmm_table_genes.tsv'),
        os.path.join(data_dir,
                     'ess_summary_test/overall_lvl_summary/default_case/Dickeya-Dickeya_zeae_A586-control-_-_-transit_hmm/hmm_table_genes.tsv'),
        os.path.join(data_dir,
                     'ess_summary_test/overall_lvl_summary/default_case/Pseudomonas-Pseudomonas_syringae_CC1557-control-_-_-transit_hmm/hmm_table_genes.tsv'),
        os.path.join(data_dir,
                     'ess_summary_test/overall_lvl_summary/default_case/Dickeya-Dickeya_dadantii_3937-chicory-_-_-transit_hmm/hmm_table_genes.tsv')
    ]
    orthogrps = os.path.join(data_dir, 'ess_summary_test/overall_lvl_summary/default_case/orthogrps.tsv')

    random.shuffle(files)
    tnphyto.build_core_overall_lvl_summary(files, orthogrps, computed_res)
    assert filecmp.cmp(expected_res, computed_res), "Computed result file & expected result file differ"


# ------------------------------------------------------------------------------#
# build_condi_strain_lvl_summary function                                       #
# ------------------------------------------------------------------------------#

def test_build_condi_strain_lvl_summary_default_case_one_condition(data_dir, tmp_path):
    expected_res = os.path.join(data_dir,
                                "condi_summary_test/strain_lvl_summary/default_case/expected_res_one_condition.tsv")
    computed_res = tmp_path / 'strain_lvl_summary.tsv'
    files = [os.path.join(data_dir,
                          'condi_summary_test/strain_lvl_summary/default_case/Dickeya-Dickeya_dadantii_3937-planta-_-_-transit_resampling/resampling_table.tsv')
             ]

    tnphyto.build_condi_strain_lvl_summary(files, computed_res)
    assert filecmp.cmp(expected_res, computed_res), "Computed result file & expected result file differ"


def test_build_condi_strain_lvl_summary_default_case_three_conditions(data_dir, tmp_path):
    expected_res = os.path.join(data_dir, "condi_summary_test/strain_lvl_summary/default_case/expected_res.tsv")
    computed_res = tmp_path / 'strain_lvl_summary.tsv'
    files = [os.path.join(data_dir,
                          'condi_summary_test/strain_lvl_summary/default_case/Dickeya-Dickeya_dadantii_3937-planta-_-_-transit_resampling/resampling_table.tsv'),
             os.path.join(data_dir,
                          'condi_summary_test/strain_lvl_summary/default_case/Dickeya-Dickeya_dadantii_3937-water-_-_-transit_resampling/resampling_table.tsv'),
             os.path.join(data_dir,
                          'condi_summary_test/strain_lvl_summary/default_case/Dickeya-Dickeya_dadantii_3937-chicory-_-_-transit_resampling/resampling_table.tsv')
             ]
    random.shuffle(files)

    tnphyto.build_condi_strain_lvl_summary(files, computed_res)
    assert filecmp.cmp(expected_res, computed_res), "Computed result file & expected result file differ"


# ------------------------------------------------------------------------------#
# build_condi_overall_lvl_summary function                                      #
# ------------------------------------------------------------------------------#

def test_build_condi_overall_lvl_summary_default_case(data_dir, tmp_path):
    expected_res = os.path.join(data_dir, "condi_summary_test/overall_lvl_summary/default_case/expected_res.tsv")
    computed_res = tmp_path / 'overall_summary.tsv'

    files = [
        os.path.join(data_dir,
                     'condi_summary_test/overall_lvl_summary/default_case/Dickeya-Dickeya_dadantii_3937-chicory-_-_-transit_resampling/resampling_table.tsv'),
        os.path.join(data_dir,
                     'condi_summary_test/overall_lvl_summary/default_case/Dickeya-Dickeya_dadantii_3937-other-_-_-transit_resampling/resampling_table.tsv'),
        os.path.join(data_dir,
                     'condi_summary_test/overall_lvl_summary/default_case/Pseudomonas-Pseudomonas_syringae_CC1557-other-_-_-transit_resampling/resampling_table.tsv'),
        os.path.join(data_dir,
                     'condi_summary_test/overall_lvl_summary/default_case/Pseudomonas-Pseudomonas_syringae_TA043-other-_-_-transit_resampling/resampling_table.tsv'),

    ]
    orthogrps = os.path.join(data_dir, 'condi_summary_test/overall_lvl_summary/default_case/orthogrps.tsv')

    random.shuffle(files)
    tnphyto.build_condi_overall_lvl_summary(files, orthogrps, computed_res)
    assert filecmp.cmp(expected_res, computed_res), "Computed result file & expected result file differ"
