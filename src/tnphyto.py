#!/usr/bin/env python3

import os
import logging
import tempfile

import pandas as pd
import re
import sys
import shutil
import json
import functools
import orthologs
import runner
import transit
import process_reads
import genbank
import sql.create_db

logger = logging.getLogger()

LOESS_SCRIPT = os.path.join(os.path.dirname(os.path.realpath(__file__)), "loess.R")


# ----------------------------------------------------------------------------------------------------------------------#
# Some usefull functions (relative to path manipulation)
# ----------------------------------------------------------------------------------------------------------------------#
def relative_path(val, start):
    """
    Convenient function to be applied on dataframe
    It will convert abspath to relpath
    """
    if isinstance(val, str) and os.path.isfile(val):  # this is a trick to apply on all cols of any datatype.
        # print(val, os.path.isfile(val))
        if os.path.isabs(val):
            return os.path.relpath(val, start=start)
    return val


def parse_path(path):
    """
    /path_to_result/{genus}-{strain}-{condition}-{rep}-{pool}-{analysis}/result.file
    #               >--------------------------------------------------<
    """
    d = dict()
    filename_sep = '-'
    fields = os.path.basename(os.path.dirname(path)).split(filename_sep)
    d['genus'] = fields[0]
    d['strain'] = fields[1]
    d['condition'] = fields[2]
    d['rep'] = fields[3]
    d['pool'] = fields[4]
    if len(fields) == 5:
        d['analysis'] = fields[5]
    else:
        d['analysis'] = '_'
    return d


def get_condition(path):
    return parse_path(path)['condition']


def is_gz_file(filepath):
    with open(filepath, 'rb') as test_f:
        return test_f.read(2) == b'\x1f\x8b'


def build_path_from(genbank, extension):
    """
    when the user provide a genbank file, the "prot_table" and "fasta" path will be generated from the gb path
    """
    filename, ext = os.path.splitext(genbank)
    return f'{filename}{extension}'


# ----------------------------------------------------------------------------------------------------------------------#
# end
# ----------------------------------------------------------------------------------------------------------------------#

class InputsTable(object):
    def __init__(self, inputs_table, fq=False, force=False):
        if not os.path.isfile(inputs_table):
            logger.fatal("The inputs table file does not exist: {}".format(inputs_table))
            sys.exit(-1)
        self.force = force  # force the conversion

        self.path = os.path.abspath(inputs_table)
        self.df = pd.read_csv(inputs_table, sep='\t', skipinitialspace=True)
        self.contains_fastq_files = fq

        self.metadata_columns = ["genus", "strain", "condition", "pool", "rep"]
        self.data_columns = list()
        self.data_columns.append("genbank")

        # we can have fastq files xor wiggle files
        if self.contains_fastq_files:
            self.data_columns.append("fastq")
        else:
            self.data_columns.append("wiggle")

        # in order to work, we use absolute path internally
        for data_col in self.data_columns:
            if data_col in self.df.columns:
                self.df[data_col] = self.df[data_col].apply(self.get_abs_path)

        logger.info("Checking the consistency of the table")
        self.check()
        logger.info("Converting genbank files (if needed)")
        self.convert()

    def _sanitaze_string(self, string):
        if isinstance(string, str):
            return re.sub(r'[^a-zA-Z0-9]', '_', string.strip())
        return string

    def get_abs_path(self, path):
        """
        path provided in the ed file are relative to the ed file.
        return an absolute path
        """
        if os.path.isabs(path):
            return os.path.realpath(path)
        inputs_table_dir, _ = os.path.split(self.path)
        return os.path.realpath(os.path.join(inputs_table_dir, path))

    def check(self):
        # check if mandatory columns exist
        logger.info("Checking mandatory columns")
        for col in self.metadata_columns + self.data_columns:
            if col not in self.df.columns:
                logger.fatal(f"The inputs table file is missing the required column: {col}")
                sys.exit(-1)

        # replace invalid characters for the following columns
        logger.info("Sanitizing strings")
        logger.debug("Columns to sanitize: %s", self.metadata_columns)
        for colname in self.metadata_columns:
            self.df[colname] = self.df[colname].apply(self._sanitaze_string)

        # assert that there is no NA values
        logger.info("Looking for missing values")
        if self.df.isnull().values.any():
            logger.fatal("The inputs table file has missing values. It's strictly forbidden.")
            sys.exit(-1)

        # check if file exists
        logger.info("Looking for wrong file")
        for index, row in self.df.iterrows():
            humand_readable_idx = index + 2  # +1 for 1-based and +1 for header
            for colname in self.data_columns:
                data = row[colname]
                # genbank col is a special case as the user can provide a genbank file or implicit (prot_table & fasta)
                if colname.lower() == "genbank":
                    if not os.path.isfile(data):
                        prot_table_path = build_path_from(data, '.prot_table')
                        fasta_path = build_path_from(data, '.fa')
                        if not (os.path.isfile(prot_table_path) and os.path.isfile(fasta_path)):
                            logger.fatal(
                                f'The "genbank" file does not exists. Plus, the "prot_table" and the "fasta" are not available. colname={colname}, line={humand_readable_idx}, path={data}')
                            sys.exit(-1)
                else:
                    if not os.path.isfile(data):
                        logger.fatal(
                            f"The inputs table file contains an invalid path. colname={colname}, line={humand_readable_idx}, path={data}")
                        sys.exit(-1)

        # check that this combination of columns are unique among the df.
        logger.info("Looking for duplicated rows")
        row_number = len(self.df.index)
        uniq_row_number = len(self.df.drop_duplicates(subset=self.metadata_columns).index)
        if row_number != uniq_row_number:
            logger.fatal("The combination of the following columns must be unique among the inputs table: {}".format(
                self.metadata_columns))
            sys.exit(-1)

        # check that the genbank files are the same for a given strain
        logger.info("Asserting that annotation files are consistent for a given strain")
        for strain, group in self.df.groupby('strain'):
            if "genbank" in self.df.columns:
                if len(set(group["genbank"])) != 1:
                    logger.fatal(f'The "genbank" files are not the same for strain: "{strain}"')
                    sys.exit(-1)

    def extra_check_on_condition(self):
        """
        the condi analysis needs some extra check
        """
        logger.info("Performing extra check on the inputs table")
        # check that all conditions have the same number of replicates within a strain.
        logger.info("Checking that all conditions have the same number of replicates within a strain")
        for strain, group in self.df.groupby('strain'):
            rep_number = None
            for condition, cgroup in group.groupby('condition'):
                if rep_number is None:
                    rep_number = len(cgroup.index)
                if rep_number != len(cgroup.index):
                    logger.fatal(
                        f"The number of replicates is not consistent between your conditions in the inputs table, strain: {strain}")
                    sys.exit(-1)

        # In order to predict condi genes, we compare a control condi to an experimental condition.
        # Check that a condition named "control" exists for each strain
        logger.info("Asserting the presence of the condition named 'control'")
        for strain, group in self.df.groupby('strain'):
            ctrl_condi_exists = False
            for condition, cgroup in group.groupby('condition'):
                if condition.lower() == 'control':
                    ctrl_condi_exists = True
            if not ctrl_condi_exists:
                logger.fatal(f'You must provide a condition called "control" for the strain: {strain}')
                sys.exit(-1)

    def convert(self):
        """
        If the user supply gb file we need to convert to prot_table & fasta
        """
        for strain, group in self.df.groupby('strain'):
            logger.info(f'Strain: {strain}')
            path = group.iloc[0]['genbank']  # get the first line only

            prot_table_path = build_path_from(path, '.prot_table')
            fasta_path = build_path_from(path, '.fa')

            # parse & convert gb file only if needed
            need_to_convert = (not os.path.exists(prot_table_path) or not os.path.exists(fasta_path))
            if self.force or need_to_convert:
                gb = genbank.GenBank(path)

                # convert to prot_table
                gb.convert_to_prottable(prot_table_path, force=self.force)
                # convert to fasta
                gb.as_fasta(fasta_path, force=self.force)
            else:
                logger.warning('The "prot_table" file already exists. No conversion will be done')
                logger.debug(f'prot_table: {prot_table_path}')
                logger.warning('The "fasta" file already exists. No conversion will be done')
                logger.debug(f'fasta: {fasta_path}')



    def write_table(self, path):
        path = os.path.abspath(path)

        relative_path_to_current_table = functools.partial(relative_path, start=os.path.dirname(path))
        rel_path_df = self.df.applymap(relative_path_to_current_table)
        rel_path_df.to_csv(path, sep='\t', index=False)


class Pipeline(object):

    def __init__(self, inputs_table_file, wkdir, rmarkdown_template, fq=False, force=False):
        self.force = force
        if os.path.exists(wkdir):
            if self.force:
                shutil.rmtree(wkdir)
            else:
                logger.fatal(f"The working directory already exists: {wkdir}")
                sys.exit(-1)

        # if fq, replace the column wiggle by fastq
        logger.info("PROCESSING THE INPUTS TABLE")
        logger.debug(f"Path: : {inputs_table_file}")
        self.it = InputsTable(inputs_table_file, fq=fq, force=self.force)
        self.orthgrp = None
        self.wkdir = wkdir
        self.sanitized_dir = os.path.join(self.wkdir, '1_sanitized_inputs')
        self.analyse_dir = os.path.join(self.wkdir, '2_analysis')
        self.report_dir = os.path.join(self.wkdir, '3_report')
        self.downstream_dir = os.path.join(self.wkdir, '4_downstream')

        # for jupyter notebook, use os.getcwd() instead of __file__
        # self.rmarkdown_tpl_dir = os.path.join(os.path.realpath(os.getcwd()), "rmarkdown", rmarkdown_template)
        self.rmarkdown_tpl_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "rmarkdown",
                                              rmarkdown_template)

        # will contain all info needed to build a report on the analysis.
        self.json_file = os.path.join(self.report_dir, 'report.json')

    def build_directory_structure(self):
        logger.debug("Building the output directory structure")
        logger.debug("Creating wkdir: %s", self.wkdir)
        os.makedirs(self.wkdir, exist_ok=False)
        logger.debug("Creating sanitized dir: %s", self.sanitized_dir)
        os.makedirs(self.sanitized_dir, exist_ok=False)
        logger.debug("Creating analyse dir: %s", self.analyse_dir)
        os.makedirs(self.analyse_dir, exist_ok=False)

        logger.debug("Copying the rmarkdown template")
        logger.debug("From: %s", self.rmarkdown_tpl_dir)
        logger.debug("To: %s", self.report_dir)
        shutil.copytree(self.rmarkdown_tpl_dir, self.report_dir)
        logger.debug("Creating downstream dir : %s", self.downstream_dir)
        os.makedirs(self.downstream_dir, exist_ok=False)

    def save_json(self, sanitized_inputs, user_params, report_params):
        """
        Save all information needed to build the rmarkdown report for the current analysis.
        """
        json_string = {}

        # save inputs files
        ortho_table_path = self.orthgrp.path if self.orthgrp is not None else None
        json_string['user_inputs'] = {'inputs_table_path': self.it.path, 'ortho_table_path': ortho_table_path}

        # save sanitized
        json_string['sanitized_inputs'] = sanitized_inputs

        # save user parameters
        json_string['user_params'] = user_params

        # save report parameters (if needed)
        json_string['report_params'] = report_params

        # save outputs
        json_string['output_dir'] = self.wkdir

        with open(self.json_file, 'w') as json_handler:
            print(json.dumps(json_string, indent=4), file=json_handler)

    def write_sanizated_inputs_table(self):
        path = os.path.join(self.sanitized_dir, os.path.basename(self.it.path))
        self.it.write_table(path)
        return path

    def knit(self):
        logger.info('GENERATING REPORT')
        # knit report
        json_file = self.json_file
        knit_cmd = f"Rscript -e 'rmarkdown::render(\"report.Rmd\");'"
        runner.run(knit_cmd, wkdir=self.report_dir, conda_env="tnphyto")

    def run_process_reads_step(self, user_params):
        # build directory structure
        self.build_directory_structure()

        # save a sanitized version of the inputs table
        sanitized_inputs_table_path = self.write_sanizated_inputs_table()

        # save as json all info needed to build the rmarkdown report
        sanitized_inputs = {'inputs_table_path': sanitized_inputs_table_path}
        report_params = {"title": "Reads Processing"}
        self.save_json(sanitized_inputs, user_params, report_params)

        # build ed for downstream analysis
        downstream_df = self.it.df.copy().drop(columns=['fastq', ])

        logger.info('PROCESSING READS')
        for idx, row in self.it.df.iterrows():
            parameters = dict()
            genus = row["genus"]
            strain = row["strain"]
            fastq = row["fastq"]
            # prot_table = row["prot_table"]
            genbank = row["genbank"]
            fasta = build_path_from(genbank, '.fa')
            condition = row["condition"]
            rep = row["rep"]
            pool = row["pool"]

            process_wkdir = os.path.join(self.analyse_dir, f'{genus}-{strain}-{condition}-r{rep}-p{pool}')
            parameters['outdir'] = process_wkdir
            parameters['fastq'] = fastq
            parameters['fasta'] = fasta

            parameters.update(user_params)

            logger.info(f"strain: {strain}, condition: {condition}, rep: {rep}")
            logger.debug("Reads file: %s", fastq)
            logger.debug("Reference file: %s", fasta)
            wig = process_reads.process_reads(**parameters)

            # update wiggle path in inputs table to facilitate downstream analysis
            downstream_df.at[idx, 'wiggle'] = wig

        # write ed for downstream analysis
        # path are relative to the location of the inputs table file
        relative_path_from_report = functools.partial(relative_path, start=self.downstream_dir)

        downstream_df = downstream_df.applymap(relative_path_from_report)
        downstream_df.to_csv(os.path.join(self.downstream_dir, 'inputs_table.tsv'), sep='\t', index=False)

        # knit report
        self.knit()

    def run_normalization_step(self):
        # build directory structure
        self.build_directory_structure()

        # save a sanitized version of the inputs table
        sanitized_inputs_table_path = self.write_sanizated_inputs_table()

        # save as json all info needed to build the rmarkdown report
        sanitized_inputs = {'inputs_table_path': sanitized_inputs_table_path}
        user_params = dict()
        report_params = {"title": "Quality control and normalization"}
        self.save_json(sanitized_inputs, user_params, report_params)

        # build ed for downstream analysis
        downstream_df = self.it.df.copy()

        # for each wiggle file we will apply loess (if requested) then ttr (if requested) normalization
        logger.info('APPLYING NORMALIZATION(S)')
        for idx, row in self.it.df.iterrows():
            parameters = dict()
            genus = row["genus"]
            strain = row["strain"]
            condition = row["condition"]
            rep = row["rep"]
            pool = row["pool"]
            wiggle = row["wiggle"]
            perform_loess_normalization = row['loess']
            perform_ttr_normalization = row['ttr']

            current_wiggle_path = wiggle
            process_wkdir = os.path.join(self.analyse_dir, f'{genus}-{strain}-{condition}-r{rep}-p{pool}')

            logger.info(f"strain: {strain}, condition: {condition}, rep: {rep}")
            if perform_loess_normalization:
                logger.info("LOESS")
                loess_wiggle_path = os.path.join(process_wkdir, 'loess.wig')
                loess_command = f'loess.R --wig "{current_wiggle_path}" --out "{loess_wiggle_path}"'
                runner.run(loess_command, wkdir=process_wkdir, conda_env="tnphyto", log=False)
                current_wiggle_path = loess_wiggle_path

            if perform_ttr_normalization:
                logger.info("TTR")
                ttr_wiggle_path = os.path.join(process_wkdir, 'ttr.wig')
                ttr_command = f'transit normalize "{current_wiggle_path}" "{ttr_wiggle_path}" -n TTR'
                runner.run(ttr_command, wkdir=process_wkdir, conda_env="transit", log=False)
                current_wiggle_path = ttr_wiggle_path

            # update wiggle path in inputs table to facilitate downstream analysis
            downstream_df.at[idx, 'wiggle'] = current_wiggle_path

        # write ed for downstream analysis
        # path are relative to the location of the inputs table file
        relative_path_from_report = functools.partial(relative_path, start=self.downstream_dir)

        downstream_df = downstream_df.applymap(relative_path_from_report)
        downstream_df.to_csv(os.path.join(self.downstream_dir, 'inputs_table.tsv'), sep='\t', index=False)

        # knit report
        self.knit()

    def run_condi_analysis_step(self, user_params):

        logger.info('PREDICTING CONDITIONALLY ESSENTIAL GENES')

        # perform some extra check on the inputs table
        self.it.extra_check_on_condition()

        # build directory structure
        self.build_directory_structure()

        # save a sanitized version of the inputs table
        sanitized_inputs_table_path = self.write_sanizated_inputs_table()

        # save as json all info needed to build the rmarkdown report
        report_params = {"title": "Conditional essential genes",
                         "nb_intervals": None,
                         "qvalue_threshold": 0.05,
                         "log2fc_threshold": 1,
                         }
        sanitized_inputs = {'inputs_table_path': sanitized_inputs_table_path}
        self.save_json(sanitized_inputs, user_params, report_params)

        # run analysis
        # predict essentiality status for all conditions
        overall_results = list()
        for strain, group in self.it.df.groupby('strain'):
            strain_results = list()
            # run transit to compare ctrl vs cond
            genus = group["genus"].iloc[0]
            controls = list()
            controls_pool = list()
            logger.info(f"Current strain: {strain}")
            for condition, cgroup in group.groupby('condition'):
                if condition.lower() == "control":
                    controls = cgroup.wiggle.tolist()
                    controls_pool = cgroup.pool.tolist()

            for condition, cgroup in group.groupby('condition'):
                if condition.lower() == "control": continue
                logger.info(f"Current comparison: 'control' vs '{condition}'")
                experimentals = cgroup.wiggle.tolist()
                experimentals_pool = cgroup.pool.tolist()

                genbank = list(set(cgroup.genbank.tolist()))
                assert len(genbank) == 1
                genbank = genbank[0]
                prot_table = build_path_from(genbank, '.prot_table')
                process_wkdir = os.path.join(self.analyse_dir, f'{genus}-{strain}-{condition}-_-_-transit_resampling')

                parameters = {"prot_table": prot_table,
                              "control_wiggles": controls, "experimental_wiggles": experimentals,
                              "control_lib": controls_pool, "experimental_lib": experimentals_pool,
                              "outdir": process_wkdir
                              }

                parameters.update(user_params)
                result = transit.resampling(**parameters)
                strain_results.append(result)

            # build summary table at strain lvl
            logger.info('Building summary table at strain level')
            tsv = os.path.join(self.analyse_dir, f'{genus}-{strain}-_-_-_-transit_resampling_strain_summary',
                               "summary.tsv")
            logger.debug(f'Summary table: {tsv}')
            parameters = dict()
            parameters['results'] = strain_results
            parameters['tsv'] = tsv
            build_condi_strain_lvl_summary(**parameters)
            overall_results.extend(strain_results)

        # build overall summary table
        logger.info('Building overall summary table')
        tsv = os.path.join(self.analyse_dir, f'_-_-_-_-_-transit_resampling_overall_summary', "summary.tsv")
        logger.debug(f'Summary table: {tsv}')
        build_condi_overall_lvl_summary(results=overall_results, ortho_grps=self.orthgrp.path, tsv=tsv)

        # knit report
        self.knit()

    def run_core_analysis_step(self, user_params):
        logger.info('PREDICTING ESSENTIAL GENES')
        # build directory structure
        self.build_directory_structure()

        # save a sanitized version of the inputs table
        sanitized_inputs_table_path = self.write_sanizated_inputs_table()

        # save as json all info needed to build the rmarkdown report
        report_params = {"title": "Core essential genes"}
        sanitized_inputs = {'inputs_table_path': sanitized_inputs_table_path}
        self.save_json(sanitized_inputs, user_params, report_params)

        # predict essentiality status for all conditions with transit
        overall_results = list()
        for strain, group in self.it.df.groupby('strain'):
            strain_results = list()
            assert len(set(group["genus"].tolist())) == 1
            genus = group["genus"].iloc[0]
            logger.info(f"Current strain: {strain}")

            for condition, cgroup in group.groupby('condition'):
                logger.info(f"Current condition: '{condition}'")
                parameters = dict()
                parameters['outdir'] = os.path.join(self.analyse_dir, f'{genus}-{strain}-{condition}-_-_-transit_hmm')
                parameters['wiggles'] = cgroup.wiggle.tolist()
                genbank = set(cgroup.genbank.tolist())
                assert len(genbank) == 1
                genbank = genbank.pop()
                prot_table = build_path_from(genbank, '.prot_table')
                parameters['prot_table'] = prot_table
                parameters.update(user_params)
                result = transit.hmm(**parameters)
                strain_results.append(result)

            # build summary table at strain lvl
            logger.info('Building summary table at strain level')
            tsv = os.path.join(self.analyse_dir, f'{genus}-{strain}-_-_-_-transit_hmm_strain_summary', "summary.tsv")
            logger.debug(f'Summary table: {tsv}')

            parameters = dict()
            parameters['results'] = strain_results
            parameters['tsv'] = tsv
            build_core_strain_lvl_summary(**parameters)
            overall_results.extend(strain_results)

        # build overall summary table
        logger.info('Building overall summary table')
        tsv = os.path.join(self.analyse_dir, f'_-_-_-_-_-transit_hmm_overall_summary', "summary.tsv")
        logger.debug(f'Summary table: {tsv}')
        build_core_overall_lvl_summary(results=overall_results, ortho_grps=self.orthgrp.path, tsv=tsv)

        # knit report
        self.knit()

    def add_orthologous_table(self, path):
        logger.info('PARSING ORTHOLOGOUS TABLE')
        self.orthgrp = orthologs.Orthologs(path)
        self.orthgrp.convert(force=self.force)


def build_condi_strain_lvl_summary(results, tsv):
    os.makedirs(os.path.dirname(tsv), exist_ok=True)

    # sort results by alphabetic order of the condition
    results = sorted(results, key=lambda p: get_condition(p))

    # parse results
    results_df = [transit.parse_resampling_results(res, index_col="#Orf", dtype=str) for res in results]

    # remove unsed columns from df
    cols = ['log2FC', 'p-value', 'Adj. p-value']  # columns to keep
    results_df = [df.filter(items=cols) for df in results_df]

    # concat all results and enable multiheader
    conditions = [get_condition(p) for p in results]

    strain_lvl_summary_df = pd.concat(results_df, axis=1, keys=conditions)

    # manage multiheader for the final result
    strain_lvl_summary_df.reset_index(inplace=True)
    strain_lvl_summary_df = strain_lvl_summary_df.rename(columns={"index": "#Orf"})

    strain_lvl_summary_df.columns = pd.MultiIndex.from_tuples(
        [('-', "#Orf") if tuple_ == ("#Orf", "") else tuple_ for tuple_ in
         strain_lvl_summary_df.columns])

    # write to tsv file
    strain_lvl_summary_df.to_csv(tsv, sep="\t", index=False)


def build_condi_overall_lvl_summary(results, ortho_grps, tsv):
    os.makedirs(os.path.dirname(tsv), exist_ok=True)

    orthologs_obj = orthologs.Orthologs(ortho_grps)
    # results = list()
    taxa = set()
    conditions = set()
    for r in results:
        d = parse_path(r)
        taxa.add((d['genus'], d['strain']))
        conditions.add(d['condition'])
    # print('taxa', taxa)
    taxa = sorted([list(taxa) for taxa in sorted(list(taxa))])
    conditions = sorted(list(conditions))

    # build header
    tuples = list()
    for taxon in taxa:
        tuples.append(['-', 'locus_tag', ] + taxon)
    tuples.append(('-', '-', '-', 'number of strains with gene'))
    for condition in conditions:
        if condition == 'control': continue
        for taxon in taxa:
            tuples.append([condition, ] + taxon + ['logfc', ])
            tuples.append([condition, ] + taxon + ['qvalue', ])

    # build df
    overall_summary_df = pd.DataFrame(list(), columns=pd.MultiIndex.from_tuples(tuples))

    # populate dataframe (one file at a time)
    for res in results:
        d = parse_path(res)
        genus = d['genus']
        strain = d['strain']
        condition = d['condition']
        if not os.path.exists(res):
            logger.error(
                f"Missing predicion file when building overall summary table for strain '{strain}', condition '{condition}'. File: {res}")
            continue

        res_df = transit.parse_resampling_results(res, dtype=str)

        for idx, row in res_df.iterrows():
            # locus_tag	gene_name	function	logFC	logCPM	PValue	q.value
            orf = row['#Orf']
            logfc = row['log2FC']
            qvalue = row['Adj. p-value']
            grp = orthologs_obj.get_orthologous_group(orf)
            if not grp:
                grp = orf
            overall_summary_df.loc[grp, ('-', 'locus_tag', genus, strain)] = orf
            overall_summary_df.loc[grp, (condition, genus, strain, 'logfc')] = logfc
            overall_summary_df.loc[grp, (condition, genus, strain, 'qvalue')] = qvalue

    # compute the "number of strains with gene" column
    # iterate over the rows and select all columns under "locus_tag"
    # slice(None) let us select all genera and all strains for the current locus_tag
    for idx, row in overall_summary_df.loc[:, ('-', 'locus_tag', slice(None), slice(None))].iterrows():
        strains_with_gene = sum([0 if pd.isna(v) else 1 for v in row.values])
        # update the df with the computed value
        overall_summary_df.at[idx, ('-', '-', '-', 'number of strains with gene')] = strains_with_gene

    overall_summary_df.reset_index(col_level=3, col_fill='-', inplace=True)
    overall_summary_df = overall_summary_df.rename(columns={"index": "ortho_grp"})

    # sort df in descending order based on "number of strains with gene"
    # then by orthogrps
    overall_summary_df.sort_values(by=[('-', '-', '-', 'number of strains with gene'), ('-', '-', '-', 'ortho_grp')],
                                   inplace=True,
                                   ascending=(False, True))

    # write results
    overall_summary_df.to_csv(tsv, sep="\t", index=False)

    # return overall_summary_df


def build_core_strain_lvl_summary(results, tsv):
    """
    From TRANSIT HMM results, build a new table.
    The informations of the condition is retrieved from the path by the function "get_condition".
    """
    os.makedirs(os.path.dirname(tsv), exist_ok=True)

    # sort results by alphabetic order of the condition
    results = sorted(results, key=lambda p: get_condition(p))

    # parse results. dtype=str to avoid conversion of any type
    results_df = [transit.parse_hmm_results(res, index_col='#ORF', dtype=str) for res in results]

    # retrieve annotations from the first df (annotations are common among df)
    annotations_df = results_df[0][['gene', 'annotation', 'TAs']].copy()

    # remove annotations from df
    cols = ['ES sites', 'GD sites', 'NE sites', 'GA sites', 'saturation', 'mean', 'call']
    results_df = [df.filter(items=cols) for df in results_df]

    # concat all results and enable multiheader
    conditions = [get_condition(p) for p in results]

    strain_lvl_summary_df = pd.concat(results_df, axis=1, keys=conditions)

    # concat the annotations with the data
    strain_lvl_summary_df = pd.concat([annotations_df, strain_lvl_summary_df], axis=1)

    # manage multiheader for the final result
    strain_lvl_summary_df.reset_index(inplace=True)
    strain_lvl_summary_df = strain_lvl_summary_df.rename(columns={"index": "#ORF"})

    strain_lvl_summary_df.columns = pd.MultiIndex.from_tuples(
        [('-', idx) if idx in ['#ORF', 'gene', 'annotation', 'TAs'] else idx for idx in
         strain_lvl_summary_df.columns])

    # write to tsv file
    strain_lvl_summary_df.to_csv(tsv, sep="\t", index=False)

    # return strain_lvl_summary_df


def build_core_overall_lvl_summary(results, ortho_grps, tsv):
    """
    From TRANSIT HMM results and an ortho table, build a new table.
    The informations like genus, strain, condition ... are retrieved from the path by the function "parse_path".
    """
    os.makedirs(os.path.dirname(tsv), exist_ok=True)

    orthologs_obj = orthologs.Orthologs(ortho_grps)
    # results = list()
    taxa = set()
    conditions = set()
    for r in results:
        d = parse_path(r)
        taxa.add((d['genus'], d['strain']))
        conditions.add(d['condition'])
    taxa = sorted([list(taxa) for taxa in sorted(list(taxa))])
    # print(taxa)
    conditions = sorted(list(conditions))

    # build header
    tuples = list()
    for taxon in taxa:
        tuples.append(['-', 'locus_tag', ] + taxon)
    tuples.append(('-', '-', '-', 'number of strains with gene'))
    for genus in sorted(list(set([taxon[0] for taxon in taxa]))):
        tuples.append(['-', 'core genome', genus, '-'])
    tuples.append(['-', 'core genome', 'all', '-'])
    for condition in conditions:
        for taxon in taxa:
            tuples.append(['conditions', condition, ] + taxon)

    # build df
    overall_summary_df = pd.DataFrame(list(), columns=pd.MultiIndex.from_tuples(tuples))

    # populate dataframe (one file at a time)
    for res in results:
        d = parse_path(res)
        genus = d['genus']
        strain = d['strain']
        condition = d['condition']
        # parse results
        if not os.path.exists(res):
            logger.error(
                f"Missing predicion file when building overall summary table for strain '{strain}', condition '{condition}'. File: {res}")
            continue
        res_df = transit.parse_hmm_results(res, dtype=str)

        for idx, row in res_df.iterrows():
            orf = row['#ORF']
            if pd.isnull(orf): continue

            call = row['call']
            grp = orthologs_obj.get_orthologous_group(orf)
            # if no ortho grp is asigned, then the ortho grp is the locus_tag itself.
            if not grp:
                grp = orf
            overall_summary_df.loc[grp, ('-', 'locus_tag', genus, strain)] = orf
            overall_summary_df.loc[grp, ('conditions', condition, genus, strain)] = call

    # compute the "number of strains with gene" column
    # iterate over the rows and select all columns under "locus_tag"
    # slice(None) let us select all genera and all strains for the current locus_tag
    for idx, row in overall_summary_df.loc[:, ('-', 'locus_tag', slice(None), slice(None))].iterrows():
        strains_with_gene = sum([0 if pd.isna(v) else 1 for v in row.values])
        # update the df with the computed value
        overall_summary_df.at[idx, ('-', '-', '-', 'number of strains with gene')] = strains_with_gene

    # for each genus retrieve retrieve the call if all strains have the same, otherwise, set to None.
    genera = sorted(list(set([taxon[0] for taxon in taxa])))

    # print(genera)
    for genus in genera:
        for idx, row in overall_summary_df.loc[:, ('conditions', slice(None), genus, slice(None))].iterrows():
            values = row.values
            unique_values = list(set(values))
            # print(values)
            if len(unique_values) == 1 and not pd.isna(unique_values[0]):
                # print(['-', 'core genome', genus, '-'])
                overall_summary_df.at[idx, ('-', 'core genome', genus, '-')] = unique_values[0]
    # do the same for all genus together.
    for idx, row in overall_summary_df.loc[:, ('conditions', slice(None), slice(None), slice(None))].iterrows():
        values = row.values
        unique_values = list(set(values))
        if len(unique_values) == 1 and not pd.isna(unique_values[0]):
            overall_summary_df.at[idx, ('-', 'core genome', 'all', '-')] = unique_values[0]

    overall_summary_df.reset_index(col_level=3, col_fill='-', inplace=True)
    overall_summary_df = overall_summary_df.rename(columns={"index": "ortho_grp"})

    # sort df in descending order based on "number of strains with gene"
    # then by orthogrps
    overall_summary_df.sort_values(by=[('-', '-', '-', 'number of strains with gene'), ('-', '-', '-', 'ortho_grp')],
                                   inplace=True,
                                   ascending=(False, True))

    # write results
    overall_summary_df.to_csv(tsv, sep="\t", index=False)
    # return overall_summary_df


if __name__ == '__main__':
    import argparse
    import configure_logger

    # import tempfile
    # logfile = tempfile.NamedTemporaryFile(prefix="tnphyto_", suffix=".log", delete=False)
    # logging.info("A more detailed log file can be found here: %s" % logfile.name)

    parser = argparse.ArgumentParser(description="TnPytho parser")

    subparsers = parser.add_subparsers(dest='command')

    # -- PROCESS READS -- #
    process_reads_parser = subparsers.add_parser(
        "process-reads",
        help="Process reads",
    )
    process_reads_parser.add_argument("-i", "--inputs-table",
                                      type=argparse.FileType('r'),
                                      help="Inputs table file",
                                      required=True)

    process_reads_parser.add_argument("-o", "--output",
                                      type=str,
                                      help="Output directory",
                                      required=True
                                      )
    process_reads_parser.add_argument("--transposon",
                                      type=str,
                                      help="Sequence of the transposon ligated to the 3' end of the reads",
                                      required=True
                                      )
    process_reads_parser.add_argument("--max-error-rate",
                                      type=float,
                                      help="Maximum allowed error rate",
                                      default=0.05
                                      )
    process_reads_parser.add_argument("--min-overlap",
                                      type=int,
                                      help="Require MINLENGTH overlap between read and transposon",
                                      default=7

                                      )
    process_reads_parser.add_argument("--minimum-length",
                                      type=int,
                                      help="Discard processed reads that are shorter than LENGTH",
                                      default=15
                                      )
    process_reads_parser.add_argument("--maximum-length",
                                      type=int,
                                      help="Discard processed reads that are longer than LENGTH",
                                      default=20
                                      )
    process_reads_parser.add_argument("--cut",
                                      type=int,
                                      # help=argparse.SUPPRESS, # do not expose this argument
                                      help="Number of bases to remove from the beginning of each read.",
                                      default=0
                                      )
    process_reads_parser.add_argument("-f", '--force',
                                      help="If the output directory already exist, delete it. Plus force the conversion for genbank & orthologous files.",
                                      action='store_true',
                                      )
    process_reads_parser.add_argument("-v", '--verbose',
                                      help="",
                                      action='store_true',
                                      )

    # -- NORMALIZE -- #
    qc_parser = subparsers.add_parser(
        "normalize",
        help="Normalize",
    )
    qc_parser.add_argument("-i", "--inputs-table",
                           type=argparse.FileType('r'),
                           help="Inputs table file",
                           required=True)
    qc_parser.add_argument("-o", "--output",
                           type=str,
                           help="Output directory",
                           required=True
                           )
    qc_parser.add_argument("-f", '--force',
                           help="If the output directory already exist, delete it. Plus force the conversion for genbank & orthologous files.",
                           action='store_true',
                           )
    qc_parser.add_argument("-v", '--verbose',
                           help="",
                           action='store_true',
                           )

    # -- CORE ANALYSIS -- #
    core_analysis_parser = subparsers.add_parser(
        "core-analysis",
        help="Core analysis",
    )

    core_analysis_parser.add_argument("-i", "--inputs-table",
                                      type=argparse.FileType('r'),
                                      help="Inputs table file",
                                      required=True)

    core_analysis_parser.add_argument("-g", "--ortho-grps",
                                      type=argparse.FileType('r'),
                                      help="Orthologous groups file",
                                      default=tempfile.NamedTemporaryFile(mode='r', prefix="empty_ortho_grps",
                                                                          suffix='.tsv'),
                                      required=False)

    core_analysis_parser.add_argument("-o", "--output",
                                      type=str,
                                      help="Output directory",
                                      required=True
                                      )
    core_analysis_parser.add_argument("-r", "--rep",
                                      type=str,
                                      help="How to handle replicates. (default: %(default)s)",
                                      default='MEAN', choices=transit.REPLICA_METHODS
                                      )

    core_analysis_parser.add_argument("-N", "--ignoreN",
                                      type=int,
                                      help="Ignore TAs occuring within given percentage (as integer) of the N terminus (default: %(default)s)",
                                      default=0
                                      )
    core_analysis_parser.add_argument("-C", "--ignoreC",
                                      type=int,
                                      help="Ignore TAs occuring within given percentage (as integer) of the C terminus (default: %(default)s)",
                                      default=0
                                      )
    core_analysis_parser.add_argument("-f", '--force',
                                      help="If the output directory already exist, delete it. Plus force the conversion for genbank & orthologous files.",
                                      action='store_true',
                                      )
    core_analysis_parser.add_argument("-v", '--verbose',
                                      help="",
                                      action='store_true',
                                      )

    # -- CONDITIONAL ANALYSIS -- #
    condi_analysis_parser = subparsers.add_parser(
        "condi-analysis",
        help="Conditional analysis",
    )

    condi_analysis_parser.add_argument("-i", "--inputs-table",
                                       type=argparse.FileType('r'),
                                       help="Inputs table file",
                                       required=True)

    condi_analysis_parser.add_argument("-g", "--ortho-grps",
                                       type=argparse.FileType('r'),
                                       help="Orthologous groups file",
                                       default=tempfile.NamedTemporaryFile(mode='r', prefix="empty_ortho_grps",
                                                                           suffix='.tsv'),
                                       required=False)

    condi_analysis_parser.add_argument("-o", "--output",
                                       type=str,
                                       help="Output directory",
                                       required=True
                                       )
    condi_analysis_parser.add_argument("-s", '--samples-number',
                                       type=int,
                                       help="The number of samples (permutations) to perform. (default: %(default)s)",
                                       default=10000
                                       )
    condi_analysis_parser.add_argument("-x", '--exclude-rows-with-zero',
                                       help="Exclude rows with zero across conditions (default: False)",
                                       action='store_true',
                                       )
    condi_analysis_parser.add_argument("-a", '--adaptative-resampling',
                                       help="Perform adaptive resampling. (default: False)",
                                       action='store_true',
                                       )
    condi_analysis_parser.add_argument("-p", '--pseudocount',
                                       type=int,
                                       help="Pseudocounts used in calculating LFC (default: %(default)s)",
                                       default=1
                                       )

    condi_analysis_parser.add_argument("-N", "--ignoreN",
                                       type=int,
                                       help="Ignore TAs occuring within given percentage (as integer) of the N terminus (default: %(default)s)",
                                       default=0
                                       )
    condi_analysis_parser.add_argument("-C", "--ignoreC",
                                       type=int,
                                       help="Ignore TAs occuring within given percentage (as integer) of the C terminus (default: %(default)s)",
                                       default=0
                                       )
    condi_analysis_parser.add_argument("-f", '--force',
                                       help="If the output directory already exist, delete it. Plus force the conversion for genbank & orthologous files.",
                                       action='store_true',
                                       )
    condi_analysis_parser.add_argument("-v", '--verbose',
                                       help="",
                                       action='store_true',
                                       )

    # -- CREATE DATABASE -- #
    createdb_parser = subparsers.add_parser(
        "create-db",
        help="Create database",
    )
    createdb_parser.add_argument("-p", "--project-directory",
                                 type=str,
                                 help="Project directory",
                                 required=True)
    createdb_parser.add_argument("-g", "--ortho-grps",
                                 type=argparse.FileType('r'),
                                 help="Orthologous groups file (TSV)",
                                 required=True)
    createdb_parser.add_argument("-o", "--output",
                                 type=str,
                                 help="The output database",
                                 required=True
                                 )
    createdb_parser.add_argument("-f", '--force',
                                 help="If the database alredy exists, delete it.",
                                 action='store_true',
                                 )
    createdb_parser.add_argument("-v", '--verbose',
                                 help="",
                                 action='store_true',
                                 )


    args = parser.parse_args()
    args.output = os.path.abspath(args.output)

    configure_logger.configure(args.verbose)
    logger.info('STARTING.')

    if args.command == "process-reads":
        p = Pipeline(args.inputs_table.name, wkdir=args.output, rmarkdown_template="process_reads_report", fq=True,
                     force=args.force)
        user_params = {"transposon": args.transposon,
                       "max_error_rate": args.max_error_rate,
                       "min_overlap": args.min_overlap,
                       "minimum_length": args.minimum_length,
                       "maximum_length": args.maximum_length,
                       "cut": args.cut}
        p.run_process_reads_step(user_params)

    elif args.command == "normalize":
        p = Pipeline(args.inputs_table.name, wkdir=args.output, rmarkdown_template="qc_report", fq=False,
                     force=args.force)
        p.run_normalization_step()

    elif args.command == "core-analysis":
        p = Pipeline(args.inputs_table.name, wkdir=args.output, rmarkdown_template="core_report", force=args.force)
        p.add_orthologous_table(args.ortho_grps.name)

        user_params = {
            "how_to_handle_replica": args.rep,
            "ignoreN": args.ignoreN,
            "ignoreC": args.ignoreC
        }
        p.run_core_analysis_step(user_params)

    elif args.command == "condi-analysis":
        p = Pipeline(args.inputs_table.name, wkdir=args.output, rmarkdown_template='condi_report', force=args.force)
        p.add_orthologous_table(args.ortho_grps.name)

        user_params = {
            "samples_number": args.samples_number,
            "exclude_rows_with_zero": args.exclude_rows_with_zero,
            "pseudocount": args.pseudocount,
            "adaptative_resampling": args.adaptative_resampling,
            "ignoreN": args.ignoreN,
            "ignoreC": args.ignoreC,
        }
        p.run_condi_analysis_step(user_params)
    elif args.command == "create-db":
        sql.create_db.create_database(args.project_directory, args.ortho_grps.name, args.output, force=args.force)
    logger.info('SUCCESS')
