#!/usr/bin/env python3

import os
import sys
import pandas as pd
import runner
import logging

logger = logging.getLogger(__name__)
REPLICA_METHODS = ['MEAN', 'SUM']


def hmm(prot_table, wiggles, how_to_handle_replica, ignoreN, ignoreC, outdir):
    # check if wiggles exist
    for wig in wiggles:
        if not os.path.isfile(wig):
            logger.fatal("Invalid path: {}".format(wig))
            sys.exit(-1)

    # check if prot_table exist
    if not os.path.isfile(prot_table):
        logger.fatal("Invalid path: {}".format(prot_table))
        sys.exit(-1)

    how_to_handle_replica = how_to_handle_replica.lower().capitalize()  # transit is expecting "Sum" or "Mean" (case dependant)
    parameters = {"wigs": ','.join(wiggles), "annotations": prot_table, "rep": how_to_handle_replica,
                  "ignoreN": ignoreN,
                  "ignoreC": ignoreC}
    cmd = 'transit hmm "{wigs}" "{annotations}" "hmm_table.tsv" -r {rep} -n nonorm -iN {ignoreN} -iC {ignoreC}'.format(
        **parameters)

    runner.run(cmd, wkdir=outdir, conda_env="transit")

    return os.path.join(outdir, "hmm_table_genes.tsv")


def resampling(prot_table, control_wiggles, experimental_wiggles, samples_number, adaptative_resampling,
               exclude_rows_with_zero, pseudocount, ignoreN, ignoreC, control_lib, experimental_lib, outdir):
    # print(control_wiggles, control_lib)
    # print(experimental_wiggles, experimental_lib)
    # print(samples_number, adaptative_resampling, exclude_rows_with_zero, pseudocount, ignoreN, ignoreC)

    if adaptative_resampling:
        adaptative_resampling = '-a'
    else:
        adaptative_resampling = ''

    if exclude_rows_with_zero:
        exclude_rows_with_zero = '-ez'
    else:
        exclude_rows_with_zero = ''

    # check if wiggles exist
    for wig in control_wiggles + experimental_wiggles:
        if not os.path.isfile(wig):
            logger.fatal("Invalid path: {}".format(wig))
            sys.exit(-1)

    # check if prot_table exist
    if not os.path.isfile(prot_table):
        logger.fatal("Invalid path: {}".format(prot_table))
        sys.exit(-1)

    control_wiggles = ','.join(control_wiggles)
    experimental_wiggles = ','.join(experimental_wiggles)

    control_lib = ''.join(control_lib)
    experimental_lib = ''.join(experimental_lib)

    cmd = f'transit resampling "{control_wiggles}" "{experimental_wiggles}" "{prot_table}" "resampling_table.tsv"'
    cmd += f' -s {samples_number} -n nonorm {adaptative_resampling} {exclude_rows_with_zero} -PC {pseudocount}'
    cmd += f' -iN {ignoreN} -iC {ignoreC} --ctrl_lib "{control_lib}" --exp_lib "{experimental_lib}"'

    runner.run(cmd, wkdir=outdir, conda_env="transit")

    return os.path.join(outdir, "resampling_table.tsv")


def parse_hmm_results(path, index_col=None, dtype=None):
    """"
    We use na_values='' and keep_default_na=False to avoid that values N/A are interpreted as NA values.
    "N/A=not analyzed" (genes with 0 TA sites) in TRANSIT.

    So, when the summary table is built for all strains (with orthologs table), we will be able to do the distinction
    between the case where the call for a gene is NA because there is no correspondence in the orthologs table
    and the case the call is NA because there is no prediction by TRANSIT.

    Pandas doc for read_table says:
    If keep_default_na is False, and na_values are specified, only the NaN values specified na_values are used for parsing.
    """
    return pd.read_table(path, sep='\t', skiprows=3, index_col=index_col, dtype=dtype, na_values='',
                         keep_default_na=False)


def parse_resampling_results(path, index_col=None, dtype=None):
    return pd.read_table(path, sep='\t', skiprows=7, index_col=index_col, dtype=dtype)


# TEST  LA FONCTION CONCAT DE PANDAS
# df1 = pd.DataFrame(
#      {
#          "A": ["A0", "A1", "A2", "A3"],
#          "B1": ["B0", "B1", "B2", "B3"],
#          "C1": ["C0", "C1", "C2", "C3"],
#          "D1": ["D0", "D1", "D2", "D3"],
# }
# )
# df1.set_index('A', verify_integrity=True, inplace=True)
#
# df2 = pd.DataFrame(
#      {
#          "A": ["A2", "A5", "A4", "A3"],
#          "B2": ["B2", "B5", "B4", "B3"],
#          "C2": ["C2", "C5", "C4", "C3"],
#          "D2": ["D2", "D5", "D4", "D3"],
#      }
#  )
# df2.set_index('A', verify_integrity=True, inplace=True)
#
# df3 = pd.DataFrame(
#      {
#          "A": ["A8", "A9", "A10", "A11"],
#          "B3": ["B8", "B9", "B10", "B11"],
#          "C3": ["C8", "C9", "C10", "C11"],
#          "D3": ["D8", "D9", "D10", "D11"],
#      }
#  )
# df3.set_index('A', verify_integrity=True, inplace=True)
#
# display(df1)
# display(df2)
# display(df3)
# pd.concat([df1, df2, df3], axis=1)


if __name__ == '__main__':
    pass
