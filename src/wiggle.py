#!/usr/bin/env python3

import numpy as np
import os
import pandas as pd
import sys
import scipy.stats


def rows2skip(wiggle):
    skiprows = list()
    with(open(wiggle, 'r')) as handler:
        for idx, line in enumerate(handler.readlines()):
            line = line.strip()
            if line[0] not in "0123456789":
                skiprows.append(idx)
    return skiprows


def parse_wiggle(wiggle):
    if not os.path.isfile(wiggle):
        sys.exit("Wiggle file does not exist: %s" % wiggle)
    skiprows = rows2skip(wiggle)
    df = pd.read_csv(wiggle, delim_whitespace=True, skiprows=skiprows, names=('pos', 'count'),
                     dtype={'pos': np.int32, 'count': np.float64})
    # df = pd.read_csv(wiggle, sep= ' ', skiprows=skiprows, names=('pos', 'count'),
    #                 dtype={'count': np.float64, 'pos': np.int32})
    if df.isnull().values.any():
        sys.exit("Something wrong happened. Wiggle file must not have any null value : %s" % wiggle)
    return df


def get_stats(wiggle):
    # print('get_stats', wiggle)
    df = parse_wiggle(wiggle)
    tas = len(df.index)
    reads = df['count'].to_numpy()
    density = np.mean(reads > 0)
    meanrd = np.mean(reads)
    nzmeanrd = np.mean(reads[reads > 0])
    nzmedianrd = np.median(reads[reads > 0])
    maxrd = np.max(reads)
    totalrd = np.sum(reads)
    skew = scipy.stats.skew(reads[reads > 0])
    kurtosis = scipy.stats.kurtosis(reads[reads > 0])

    return tas, density, meanrd, nzmeanrd, nzmedianrd, maxrd, totalrd, skew, kurtosis


def write_stats(wiggles, outfile):
    outdir = os.path.dirname(outfile)
    os.makedirs(outdir, exist_ok=True)
    header = (
        'File',
        'TAs',
        'Density',
        'Mean Read',
        'NZMean Read',
        'NZMedian Read',
        'Max Read',
        'Total Reads',
        'Skew',
        'Kurtosis',
    )
    with open(outfile, 'w') as handler:
        print('\t'.join(header), file=handler)
        for w in wiggles:
            (tas, density, meanrd, nzmeanrd, nzmedianrd, maxrd, totalrd, skew, kurtosis) = get_stats(w)
            filename = w
            tas = "%d" % tas
            density = "%1.1f" % (density * 100.0)
            meanrd = "%1.1f" % meanrd
            nzmeanrd = "%1.1f" % nzmeanrd
            nzmedianrd = "%1.1f" % nzmedianrd
            maxrd = "%d" % maxrd
            totalrd = "%d" % totalrd
            skew = "%1.1f" % skew
            kurtosis = "%1.1f" % kurtosis
            values = (filename, tas, density, meanrd, nzmeanrd, nzmedianrd, maxrd, totalrd, skew, kurtosis)
            print('\t'.join(values), file=handler)


if __name__ == '__main__':
    import os

    f = list()
    d = "/tmp/"
    for file in os.listdir(d):
        if file.endswith(".wig"):
            f.append(os.path.join(d, file))
    write_stats(f, os.path.join(d, 'stats.tsv'))
